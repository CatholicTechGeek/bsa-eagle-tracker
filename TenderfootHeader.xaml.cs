﻿using System;
using System.Collections.Generic;
using System.Linq;
//using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Microsoft.Phone.Controls;
using System.Xml;
using System.Xml.Linq;
using System.IO.IsolatedStorage;
//using System.IO;
using CustomControlLibray;

namespace BSA_Eagle_Tracker
{
    public partial class TenderfootHeader : PhoneApplicationPage
    {
        private List<Req> requirementlist;
        private static XDocument loadeddata;

        public TenderfootHeader()
        {
            InitializeComponent();
            loadeddata = StorageHelper.getSettingsXml("scout_settings.xml");
            requirementlist = getRequirementList();
            setRequirements();
        }

        public static List<Req> getRequirementList()
        {
            List<Req> reqlist = new List<Req>();
            //XDocument loadeddata = XDocument.Load("scout_settings.xml");

            /*
            var data = from query in loadeddata.Element("Tenderfoot").Descendants()
                       select new Req
                       {
                           reqid = (string)query.Attribute("id"),
                           RequirementText = (string)query.Element("req"),
                           RequirementFinished = bool.Parse(query.Attribute("used").Value)
                           
                       };
            */
            if (loadeddata == null)
            {
                loadeddata = StorageHelper.getSettingsXml("scout_settings.xml");
            }
            var data = from query in loadeddata.Element("Scout").Element("Ranks").Element("Tenderfoot").Element("Requirements").Nodes()
                       select query;

            foreach (XElement xe in data)
            {
                Req tempreq;
                bool tempstatus = bool.Parse(xe.Attribute("done").Value);
                if (xe.HasElements == true)
                {
                    //x
                    List<SubItem> elementsubitems = new List<SubItem>();
                    foreach (XElement subxe in xe.Nodes())
                    {
                        //x
                        if (subxe.Name.LocalName.Equals("check"))
                        {
                            bool ischecked = bool.Parse(subxe.Attribute("checked").Value);
                            if (!(subxe.Attribute("width") == null))
                            {
                                //x
                                SubItem tempsubitem = new SubItem(subxe.Name.LocalName, xe.Attribute("id").Value, subxe.Value, subxe.Attribute("margin").Value, subxe.Attribute("height").Value, subxe.Attribute("width").Value, ischecked);
                                elementsubitems.Add(tempsubitem);
                            }
                            else
                            {
                                //x
                                SubItem tempsubitem = new SubItem(subxe.Name.LocalName, xe.Attribute("id").Value, subxe.Value, subxe.Attribute("margin").Value, subxe.Attribute("height").Value, ischecked);
                                elementsubitems.Add(tempsubitem);
                            }
                        }
                        else if (subxe.Name.LocalName.Equals("textentry"))
                        {
                            SubItem tempsubitem = new SubItem(subxe.Name.LocalName, xe.Attribute("id").Value, subxe.Value, subxe.Attribute("margin").Value, subxe.Attribute("height").Value, subxe.Attribute("entry").Value);
                            elementsubitems.Add(tempsubitem);
                        }
                        else
                        {
                            SubItem tempsubitem = new SubItem(subxe.Name.LocalName, xe.Attribute("id").Value, subxe.Value, subxe.Attribute("margin").Value, subxe.Attribute("height").Value);
                            elementsubitems.Add(tempsubitem);
                        }
                    }
                    tempreq = new Req(xe.Attribute("id").Value, xe.Attribute("desc").Value, tempstatus, xe.Attribute("margin").Value, xe.Attribute("height").Value, elementsubitems);
                    reqlist.Add(tempreq);
                }
                else
                {
                    tempreq = new Req(xe.Attribute("id").Value, xe.Attribute("desc").Value, tempstatus, xe.Attribute("margin").Value, xe.Attribute("height").Value);
                    reqlist.Add(tempreq);
                }
            }
            return reqlist;
        }

        private void setRequirements()
        {
            //x
            int dropdownnum = ContentPanel.Children.Count - 1;
            reqdropdown1.Checkboxid = "Requirement " + requirementlist[0].RequirementId;
            reqdropdown1.descriptionstring = requirementlist[0].RequirementText;
            if (requirementlist[0].finished == true)
            {
                reqdropdown1.Checkedoff = true;
            }

            if (requirementlist[0].hasSubItem())
            {
                //x
                List<SubItem> subitemvalues = requirementlist[0].getSubItemList();
                SolidColorBrush subitembrush = new SolidColorBrush(Colors.White);
                //ScrollViewer subitemviewer = new ScrollViewer();
                //StackPanel subitempanel = new StackPanel();
                for (int j = 0; j < subitemvalues.Count; j++)
                {
                    //x
                    if (subitemvalues[j].typestring.Equals("check"))
                    {
                        CheckBox tempcheck = new CheckBox();
                        tempcheck.Content = subitemvalues[j].descstring;
                        tempcheck.Name = subitemvalues[j].subitemid;
                        tempcheck.Height = subitemvalues[j].descriptionheight;
                        tempcheck.HorizontalAlignment = HorizontalAlignment.Left;
                        //firstblock.Margin = new Thickness(14,163,0,0);
                        //tempcheck.Margin = subitemvalues[j].getMargin();
                        tempcheck.VerticalAlignment = VerticalAlignment.Top;
                        //tempcheck.Width = 300;
                        tempcheck.Foreground = subitembrush;
                        tempcheck.Background = subitembrush;
                        if (subitemvalues[j].isSelected == true)
                        {
                            tempcheck.IsChecked = true;
                        }
                        reqdropdown1.addToDropdown(tempcheck);
                        //subitempanel.Children.Add(tempcheck);
                    }
                }
            }

            //loop for remaining items
            for (int i = 1; i < requirementlist.Count; i++)
            {
                ButtonDropdown tempbuttondrop = new ButtonDropdown();
                tempbuttondrop.HorizontalAlignment = HorizontalAlignment.Left;
                tempbuttondrop.VerticalAlignment = VerticalAlignment.Top;
                tempbuttondrop.Checkboxid = "Requirement " + requirementlist[i].RequirementId;
                tempbuttondrop.descriptionstring = requirementlist[i].RequirementText;
                if (requirementlist[i].finished == true)
                {
                    tempbuttondrop.Checkedoff = true;
                }
                /*
                tempblock.Height = requirementlist[i].descriptionheight;
                tempblock.HorizontalAlignment = HorizontalAlignment.Left;
                //tempblock.Margin = new Thickness(14, 163, 0, 0);
                tempblock.Margin = requirementlist[i].getMargin();
                tempblock.VerticalAlignment = VerticalAlignment.Top;
                tempblock.Width = 450;
                tempblock.TextWrapping = TextWrapping.Wrap;
                Grid tempgrid = new Grid();
                tempgrid.Children.Add(tempblock);
                 */
                if (requirementlist[i].hasSubItem())
                {
                    //x
                    List<SubItem> subitemvalues = requirementlist[i].getSubItemList();
                    SolidColorBrush subitembrush = new SolidColorBrush(Colors.White);

                    for (int j = 0; j < subitemvalues.Count; j++)
                    {
                        //x
                        if (subitemvalues[j].typestring.Equals("check"))
                        {
                            CheckBox tempcheck = new CheckBox();
                            tempcheck.Content = subitemvalues[j].descstring;
                            tempcheck.Name = subitemvalues[j].subitemid;
                            tempcheck.Height = subitemvalues[j].descriptionheight;
                            tempcheck.HorizontalAlignment = HorizontalAlignment.Left;
                            //firstblock.Margin = new Thickness(14,163,0,0);
                            // tempcheck.Margin = subitemvalues[j].getMargin();
                            tempcheck.VerticalAlignment = VerticalAlignment.Top;
                            tempcheck.Width = subitemvalues[j].descriptionwidth;
                            tempcheck.Foreground = subitembrush;
                            tempcheck.Background = subitembrush;
                            if (subitemvalues[j].isSelected == true)
                            {
                                tempcheck.IsChecked = true;
                            }
                            tempbuttondrop.addToDropdown(tempcheck);
                        }
                        else if (subitemvalues[j].typestring.Equals("label"))
                        {
                            //x
                            TextBlock continueblock = new TextBlock();
                            continueblock.Text = subitemvalues[j].descstring;
                            continueblock.Name = subitemvalues[j].subitemid;
                            continueblock.Height = subitemvalues[j].descriptionheight;
                            continueblock.HorizontalAlignment = HorizontalAlignment.Left;
                            //firstblock.Margin = new Thickness(14,163,0,0);
                            //continueblock.Margin = subitemvalues[j].getMargin();
                            continueblock.VerticalAlignment = VerticalAlignment.Top;
                            continueblock.Width = 375;
                            continueblock.Foreground = subitembrush;
                            continueblock.FontSize = 22;
                            tempbuttondrop.addToDropdown(continueblock);

                        }
                        else if (subitemvalues[j].typestring.Equals("textentry"))
                        {
                            //x
                            TextBox entrybox = new TextBox();
                            TextBlock entryheader = new TextBlock();
                            entrybox.Name = subitemvalues[j].subitemid;
                            entryheader.Text = subitemvalues[j].descstring;
                            entryheader.Height = subitemvalues[j].descriptionheight;
                            entrybox.Height = subitemvalues[j].descriptionheight;
                            entryheader.Width = 185;
                            entrybox.Width = 250;
                            entryheader.Foreground = subitembrush;
                            //entryheader.Margin = subitemvalues[j].getMargin();
                            //entrybox.Margin = new Thickness(entryheader.Margin.Left, (entryheader.Margin.Top + 20), entryheader.Margin.Right, entryheader.Margin.Bottom);
                            if (!subitemvalues[j].input.Equals(" "))
                            {
                                entrybox.Text = subitemvalues[j].input;
                            }
                            entryheader.HorizontalAlignment = HorizontalAlignment.Left;
                            entrybox.HorizontalAlignment = HorizontalAlignment.Left;
                            entryheader.VerticalAlignment = VerticalAlignment.Top;
                            entrybox.VerticalAlignment = VerticalAlignment.Top;
                            StackPanel entrypanel = new StackPanel();
                            entrypanel.Orientation = System.Windows.Controls.Orientation.Horizontal;
                            entrypanel.Children.Add(entryheader);
                            entrypanel.Children.Add(entrybox);
                            tempbuttondrop.addToDropdown(entrypanel);
                        }
                    }
                }
                //add new dropdownbox to panel
                //int childnum = ContentPanel.Children.Count;
                //ButtonDropdown margindropdown = ContentPanel.Children.Cast<ButtonDropdown>().ElementAt(dropdownnum);
                //tempbuttondrop.Margin = new Thickness(margindropdown.Margin.Left, (margindropdown.Margin.Top + margindropdown.Height + 20), margindropdown.Margin.Right, margindropdown.Margin.Bottom);
                reqpanel.Children.Add(tempbuttondrop);
                dropdownnum++;
                //the end
            }
        }

        protected override void OnNavigatedFrom(System.Windows.Navigation.NavigationEventArgs e)
        {
            saveState();
        }

        public void saveState()
        {
            int reqnum = int.Parse(loadeddata.Element("Scout").Element("Ranks").Element("Tenderfoot").Element("Requirements").Attribute("num").Value);
            int completecount = 0;

            using (IsolatedStorageFile appstore = IsolatedStorageFile.GetUserStoreForApplication())
            {
                //using (IsolatedStorageFileStream xmlstream = appstore.CreateFile("scout_settings.xml"))
                using (IsolatedStorageFileStream xmlstream = new IsolatedStorageFileStream("scout_settings.xml", System.IO.FileMode.Create, System.IO.FileAccess.ReadWrite, appstore))
                {
                    //x
                    int n = 0;
                    ButtonDropdown[] saveitems = reqpanel.Children.Cast<ButtonDropdown>().ToArray();
                    foreach (XElement xe in loadeddata.Element("Scout").Element("Ranks").Element("Tenderfoot").Element("Requirements").Nodes())
                    {
                        //x
                        //string realid = saveitems[n].Checkboxid.Substring(12,(saveitems[n].Checkboxid.Length - 12));
                        if (saveitems[n].Checkedoff == true)
                        {
                            xe.Attribute("done").Value = "true";
                            completecount++;
                        }
                        else
                        {
                            xe.Attribute("done").Value = "false";
                        }

                        //handle any subitems
                        if (xe.HasElements == true)
                        {
                            StackPanel savepanel = saveitems[n].getItemsPanel();

                            //make subcount start at 1 since 1st item is always textblock
                            int subcount = 1;

                            foreach (XElement subxe in xe.Nodes())
                            {
                                if (subxe.Name.LocalName.Equals("check"))
                                {
                                    //x
                                    bool selected = false;
                                    if (savepanel.Children[subcount] is CheckBox)
                                    {
                                        //x
                                        CheckBox savecheckbox = (CheckBox)savepanel.Children[subcount];
                                        if (savecheckbox.IsChecked == true)
                                        {
                                            selected = true;
                                        }
                                        if (selected == true)
                                        {
                                            subxe.Attribute("checked").Value = "true";
                                        }
                                        else
                                        {
                                            subxe.Attribute("checked").Value = "false";
                                        }
                                    }
                                }
                                else if (subxe.Name.LocalName.Equals("textentry"))
                                {
                                    if (savepanel.Children[subcount] is TextBox)
                                    {
                                        TextBox savetextbox = (TextBox)savepanel.Children[subcount];
                                        if (!(savetextbox.Text.Equals(" ") || savetextbox.Text.Equals("")))
                                        {
                                            subxe.Attribute("entry").Value = savetextbox.Text;
                                        }
                                    }
                                }
                                subcount++;
                            }
                        }

                        if ((n + 1) < saveitems.Length)
                        {
                            n++;
                        }
                    }
                    /*
                    if (completecount == reqnum && loadeddata.Element("Scout").Element("information").Element("currentrank").Value.Equals("Scout"))
                    {
                        loadeddata.Element("Scout").Element("information").Element("currentrank").Value = "Tenderfoot";
                    }
                    else if (loadeddata.Element("Scout").Element("information").Element("currentrank").Value.Equals("Tenderfoot"))
                    {
                        loadeddata.Element("Scout").Element("information").Element("currentrank").Value = loadeddata.Element("Scout").Element("information").Element("previousrank").Value;
                    }
                     */
                    loadeddata.Save(xmlstream);
                    xmlstream.Close();
                }
            }
        }
    }
}