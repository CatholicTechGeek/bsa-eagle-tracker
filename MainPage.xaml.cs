﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Microsoft.Phone.Controls;
using System.Xml.Linq;
using System.Windows.Navigation;
using System.IO.IsolatedStorage;
using System.Text;

namespace BSA_Eagle_Tracker
{
    public partial class MainPage : PhoneApplicationPage
    {
        //public IEnumerable<string> ranklist { get; private set; }
        private List<string> scoutranks;
        private string currentrank;
        private string birthday;
        public bool configtried = false;
        //private string[] scoutranknames = { "Tenderfoot", "Second Class", "First Class", "Star", "Life", "Eagle" };

        // Constructor
        public MainPage()
        {
            InitializeComponent();

            // Set the data context of the listbox control to the sample data
            //DataContext = App.ViewModel;

            /*
            scoutranks = setRankBlocks(scoutranknames);
            //scoutranks.Add("merit badges");
            //ranklist = scoutranks;
            this.DataContext = this;
            rankpicker.ItemsSource = scoutranks;
            rankpicker.Height = 300;
             */

            scoutranks = new List<string>();
            scoutranks.Add("Select Rank");
            scoutranks.Add("Tenderfoot");
            scoutranks.Add("Second Class");
            scoutranks.Add("First Class");
            scoutranks.Add("Star");
            //scoutranks.Add("First Class");
            //scoutranks.Add("Star");
            scoutranks.Add("Life");
            scoutranks.Add("Eagle");
            //scoutranks.Add("merit badges");
            //ranklist = scoutranks;
            this.DataContext = this;
            rankpicker.ItemsSource = scoutranks;
            rankpicker.Height = 300;
           
            //this.Loaded += new RoutedEventHandler(MainPage_Loaded);
        }

        /*
        private TextBlock[] setRankBlocks(string[] rankarray)
        {
            //x
            TextBlock[] blocks = new TextBlock[rankarray.Length];
            for (int j = 0; j < rankarray.Length; j++)
            {
                blocks[j] = new TextBlock();
                blocks[j].Text = rankarray[j];
                blocks[j].FontSize = 22;
            }
            return blocks;
        }
         * */

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            //x
            //bool configfound;
            using (IsolatedStorageFile myStore = IsolatedStorageFile.GetUserStoreForApplication())
            {
                if (!myStore.FileExists("scout_settings.xml"))
                {
                    //x;
                    //StorageHelper.copyToIsolatedStorage("scout_data.xml", "scout_settings.xml");
                    if (configtried == true)
                    {
                        //x
                        showDataErrorMessage();
                    }
                    else
                    {
                        configtried = true;
                        NavigationService.Navigate(new Uri("/FirstSetup.xaml?", UriKind.Relative));
                    }
                }
                else
                {
                    //x
                    setInformation();
                    rankblock.Text = currentrank;
                    birthdayblock.Text = birthday;
                }
            }
            rankpicker.SelectedIndex = 0;
        }

        public void showDataErrorMessage()
        {
            //x
            while (ContentPanel.Children.Count > 0)
            {
                ContentPanel.Children.RemoveAt(0);
            }
            TextBlock errorblock1 = new TextBlock();
            TextBlock errorblock2 = new TextBlock();
            errorblock1.Text = "Data Error";
            //errorblock2.Text = "Configuration File does not exist. Please Exit and restart and try again.";
            errorblock2.Text = "Configuration has not been set. Please Exit and restart and try again.";
            errorblock1.HorizontalAlignment = HorizontalAlignment.Center;
            errorblock2.HorizontalAlignment = HorizontalAlignment.Left;
            errorblock1.Margin = new Thickness(114, 67, 147, 0);
            errorblock2.Margin = new Thickness(24, 147, 0, 0);
            errorblock1.VerticalAlignment = VerticalAlignment.Top;
            errorblock2.VerticalAlignment = VerticalAlignment.Top;
            errorblock1.FontSize = 28;
            errorblock2.FontSize = 22;
            errorblock2.Width = 420;
            errorblock2.Height = 200;
            errorblock2.TextWrapping = TextWrapping.Wrap;
            //errorblock2.
            ContentPanel.Children.Add(errorblock1);
            ContentPanel.Children.Add(errorblock2);
        }

        private string determineCountdown(string datexmlstring)
        {
            StringBuilder countdownbuilder = new StringBuilder();
            int monthcount = 0;
            int yearcount = 0;
            int daycount = 0;
            string countdownstring = " ";

            //get months, weeks, and days
            int datestringpos = 0;
            int sectionpos = 1;
            while (datestringpos < datexmlstring.Length)
            {
                //x
                if (datexmlstring[datestringpos] == '/')
                {
                    //countdownbuilder.Append(datexmlstring[datestringpos]);
                    switch (sectionpos)
                    {
                        case 1: monthcount = Convert.ToInt32(countdownbuilder.ToString()); break;
                        case 2: daycount = Convert.ToInt32(countdownbuilder.ToString()); break;
                    }
                    if (datestringpos == datexmlstring.Length)
                    {
                        //x
                        break;
                    }
                    else
                    {
                        sectionpos++;
                        countdownbuilder = new StringBuilder();
                    }
                }
                else
                {
                    if (datestringpos == (datexmlstring.Length - 1))
                    {
                        countdownbuilder.Append(datexmlstring[datestringpos]);
                        yearcount = Convert.ToInt32(countdownbuilder.ToString());
                    }
                    else
                    {
                        countdownbuilder.Append(datexmlstring[datestringpos]);
                    }
                }
                datestringpos++;
            }

            //determine time left
            if ((DateTime.Now.Year - yearcount) < 17)
            {
                countdownstring = (18-(DateTime.Now.Year - yearcount)) + " years";
            }
            else
            {
                //x
                countdownstring = getDayDifference(monthcount, DateTime.Now.Month, daycount, DateTime.Now.Day) + " days";
            }

            return countdownstring;
            //return countdownbuilder.ToString();
        }

        private int getDayCount(int monthnumber)
        {
            int[] thirtydays = { 9, 4, 6, 11 };
            //int[] thirtyonedays = { 1, 3, 5, 7, 8, 10, 12 };
            int[] twentyeightdays = { 2 };
            int numberofdays;

            if (thirtydays.Contains(monthnumber))
            {
                numberofdays = 30;
            }
            else if (twentyeightdays.Contains(monthnumber))
            {
                numberofdays = 28;
            }
            else
            {
                numberofdays = 31;
            }
            return numberofdays;
        }

        private int getDayDifference(int expectedmonth, int currentmonth, int expectedday, int currentday)
        {
            //x
            int daydifference;

            int monthdifference = Math.Abs(expectedmonth - currentmonth);
            if (monthdifference <= 1)
            {
                //x
                daydifference = (expectedday + getDayCount(expectedmonth)) - currentday; 
            }
            else {
                int tempdifference = currentday;
                while (currentmonth < (expectedmonth - 1))
                {
                    //x
                    tempdifference = tempdifference + getDayCount(currentmonth);
                    currentmonth++;
                }
                daydifference = ((expectedday + getDayCount(expectedmonth)) - currentday) + tempdifference; 
            }

            return daydifference;
        }

        private string getCountdown(string datexmlstring)
        {
            //first, get our dates
            string countdownstring = "";
            string[] datedata = datexmlstring.Split('/');
            DateTime birthday = new DateTime(int.Parse(datedata[2]), int.Parse(datedata[0]), int.Parse(datedata[1]));
            DateTime eagledeadline = birthday.AddYears(18);

            int yearvalue;
            if (DateTime.Now.Month > eagledeadline.Month)
            {
                yearvalue = DateTime.Now.Year + 1;
            }
            else
            {
                yearvalue = DateTime.Now.Year;
            }
            int yeardifference = eagledeadline.Year - yearvalue;
            if (yeardifference > 1)
            {
                countdownstring = yeardifference + " years";
            }
            else
            {
                //x
                int monthvalue;
                if (DateTime.Now.Month > eagledeadline.Month)
                {
                    monthvalue = eagledeadline.Month + 12;
                }
                else
                {
                    monthvalue = eagledeadline.Month;
                }

                int monthdifference;
                if (eagledeadline.Day < birthday.Day)
                {
                    monthdifference = monthvalue - (DateTime.Now.Month + 1);
                }
                else
                {
                    monthdifference = monthvalue - DateTime.Now.Month;
                }
                
                if (monthdifference < 1)
                {
                    countdownstring = "< 1 month";
                }
                else
                {
                    countdownstring = monthdifference + "months";
                }


            }
            return countdownstring;

        }

        private void setInformation()
        {
            //x
            XDocument loadeddata = StorageHelper.getSettingsXml("scout_settings.xml");
            nameblock.Text = loadeddata.Element("Scout").Element("information").Element("name").Value;
            //currentrank = "Current Rank: " + checkRank(loadeddata.Element("Scout").Element("information").Element("currentrank").Value);
            currentrank = "Current Rank: " + loadeddata.Element("Scout").Element("information").Element("currentrank").Value;
            //birthday = "Time Before Eagle: " + determineCountdown(loadeddata.Element("Scout").Element("information").Element("birthday").Value);
            birthday = "Time Before Eagle: " + getCountdown(loadeddata.Element("Scout").Element("information").Element("birthday").Value);
            /*
            var data = from query in loadeddata.Element("Scout").Element("Ranks").Element("Tenderfoot").Element("Requirements").Nodes()
                       select query;

            foreach (XElement xe in data)
            {
                tempreq = new Req(xe.Attribute("id").Value, xe.Attribute("desc").Value, tempstatus, xe.Attribute("margin").Value, xe.Attribute("height").Value);
                    reqlist.Add(tempreq);
            }
             */ 
        }


        private void rankpicker_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            //ListPickerItem tempitem = (ListPickerItem)sender;
            switch (rankpicker.SelectedIndex)
            {
                case 1: NavigationService.Navigate(new Uri("/TenderfootHeader.xaml?", UriKind.Relative)); break;
                //case 1: NavigationService.Navigate(new Uri("/Tenderfoot.xaml?", UriKind.Relative)); break;
                case 2: NavigationService.Navigate(new Uri("/SecondClass.xaml?", UriKind.Relative)); break;
                //case 2: NavigationService.Navigate(new Uri("/SecondClassHeader.xaml?", UriKind.Relative)); break;
                case 3: NavigationService.Navigate(new Uri("/SingleView.xaml?", UriKind.Relative)); break;
                case 4: NavigationService.Navigate(new Uri("/StarSingleView.xaml?", UriKind.Relative)); break;
                //case 3: NavigationService.Navigate(new Uri("/FirstClass.xaml?", UriKind.Relative)); break;
                //case 4: NavigationService.Navigate(new Uri("/Star.xaml?", UriKind.Relative)); break;
                case 5: NavigationService.Navigate(new Uri("/Life.xaml?", UriKind.Relative)); break;
                case 6: NavigationService.Navigate(new Uri("/Eagle.xaml?", UriKind.Relative)); break;
                //case 6: NavigationService.Navigate(new Uri("/EagleHeader.xaml?", UriKind.Relative)); break;
                //case 7: NavigationService.Navigate(new Uri("/MeritBadges.xaml?", UriKind.Relative)); break;
                //case 7: NavigationService.Navigate(new Uri("/SingleView.xaml?", UriKind.Relative)); break;
                //case 8: NavigationService.Navigate(new Uri("/StarSingleView.xaml?", UriKind.Relative)); break;
                //case 9: NavigationService.Navigate(new Uri("/MeritBadges.xaml?", UriKind.Relative)); break;
            }
        }

        private void resetbutton_Click(object sender, RoutedEventArgs e)
        {
            StorageHelper.deleteSettings("scout_settings.xml");
        }

        /*
        public string checkRank(string startrank)
        {
            bool rankcomplete = true;
            //XDocument rankcheck = StorageHelper.getSettingsXml("scout_settings");
            Req[] rankreqlist;

            //grab the requirements for the next rank to check for completion
            if (startrank.Equals("Scout")) {
                rankreqlist = TenderfootHeader.getRequirementList().ToArray();
            }
            else if (startrank.Equals("Tenderfoot"))
            {
                rankreqlist = SecondClass.getRequirementList().ToArray();
            }
            else if (startrank.Equals("Second Class"))
            {
                rankreqlist = SingleView.getRequirementList().ToArray();
            }
            else if (startrank.Equals("First Class"))
            {
                rankreqlist = StarSingleView.getRequirementList().ToArray();
            }
            else if (startrank.Equals("Star"))
            {
                rankreqlist = Life.getRequirementList().ToArray();
            }
            else
            {
                rankreqlist = Eagle.getRequirementList().ToArray();
            }

            //rather than starting at false and checking for true, we start at true and check for false
            int i = 0;
            while (rankcomplete == true)
            {
                //x
                if (rankreqlist[i].finished == false)
                {
                    rankcomplete = false;
                }
                if ((i + 1) < rankreqlist.Length)
                {
                    i++;
                }
            }

            //if rank is completed or rank is eagle, check next rank level
            if (rankcomplete == false || startrank.Equals("Eagle"))
            {
                //x
                return startrank;
            }
            else
            {
                //x
                if (startrank.Equals("Scout"))
                {
                    return checkRank("Tenderfoot");
                }
                else if (startrank.Equals("Tenderfoot"))
                {
                    return checkRank("Second Class");
                }
                else if (startrank.Equals("Second Class"))
                {
                    return checkRank("First Class"); ;
                }
                else if (startrank.Equals("First Class"))
                {
                    return checkRank("Star");
                }
                else if (startrank.Equals("Star"))
                {
                    return checkRank("Life");
                }
                else
                {
                    return checkRank("Eagle");
                }
            }
        }
         */
    }
}