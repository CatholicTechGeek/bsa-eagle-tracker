﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Xml.Linq;

namespace BSA_Eagle_Tracker
{
    public class MeritBadge
    {
        private string meritbadgename;
        private bool requiredforeagle;
        private bool completed;
        private string recievedrank;

        public string badgeName
        {
            get { return meritbadgename; }
            private set { meritbadgename = value; }
        }

        public bool eagleRequired
        {
            get { return requiredforeagle; }
            private set { requiredforeagle = value; }
        }

        public bool recieved
        {
            get { return completed; }
            set { completed = value; }
        }

        public string rankrecieved
        {
            get { return recievedrank; }
            private set { recievedrank = value; }
        }

        public MeritBadge(string nameofbadge, bool neededforeagle, bool finishedornot, string rankrecivedat = " ")
        {
            meritbadgename = nameofbadge;
            requiredforeagle = neededforeagle;
            completed = finishedornot;
            if (!rankrecivedat.Equals(" ")) {
                recievedrank = rankrecivedat;
            }
        }

        public MeritBadge()
        {
            //x
        }
    }
}
