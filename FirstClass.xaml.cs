﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Microsoft.Phone.Controls;
using System.Xml.Linq;
using System.IO.IsolatedStorage;
using System.IO;

namespace BSA_Eagle_Tracker
{
    public partial class FirstClass : PhoneApplicationPage
    {
        private List<Req> requirementlist;
        private static XDocument loadeddata;

        public FirstClass()
        {
            InitializeComponent();
            loadeddata = StorageHelper.getSettingsXml("scout_settings.xml");
            requirementlist = getRequirementList();
            setRequirements();
        }

        public static List<Req> getRequirementList()
        {
            List<Req> reqlist = new List<Req>();
            //XDocument loadeddata = XDocument.Load("scout_settings.xml");

            /*
            var data = from query in loadeddata.Element("Tenderfoot").Descendants()
                       select new Req
                       {
                           reqid = (string)query.Attribute("id"),
                           RequirementText = (string)query.Element("req"),
                           RequirementFinished = bool.Parse(query.Attribute("used").Value)
                           
                       };
            */
            var data = from query in loadeddata.Element("Scout").Element("Ranks").Element("FirstClass").Element("Requirements").Nodes()
                       select query;

            foreach (XElement xe in data)
            {
                Req tempreq;
                bool tempstatus = bool.Parse(xe.Attribute("done").Value);
                if (xe.HasElements == true)
                {
                    //x
                    List<SubItem> elementsubitems = new List<SubItem>();
                    foreach (XElement subxe in xe.Nodes())
                    {
                        //x
                        if (subxe.Name.LocalName.Equals("check"))
                        {
                            bool ischecked = bool.Parse(subxe.Attribute("checked").Value);
                            if (!(subxe.Attribute("width") == null))
                            {
                                //x
                                SubItem tempsubitem = new SubItem(subxe.Name.LocalName, xe.Attribute("id").Value, subxe.Value, subxe.Attribute("margin").Value, subxe.Attribute("height").Value, subxe.Attribute("width").Value, ischecked);
                                elementsubitems.Add(tempsubitem);
                            }
                            else
                            {
                                //x
                                SubItem tempsubitem = new SubItem(subxe.Name.LocalName, xe.Attribute("id").Value, subxe.Value, subxe.Attribute("margin").Value, subxe.Attribute("height").Value, ischecked);
                                elementsubitems.Add(tempsubitem);
                            }
                        }
                        else if (subxe.Name.LocalName.Equals("textentry"))
                        {
                            SubItem tempsubitem = new SubItem(subxe.Name.LocalName, xe.Attribute("id").Value, subxe.Value, subxe.Attribute("margin").Value, subxe.Attribute("height").Value, subxe.Attribute("entry").Value);
                            elementsubitems.Add(tempsubitem);
                        }
                        else
                        {
                            SubItem tempsubitem = new SubItem(subxe.Name.LocalName, xe.Attribute("id").Value, subxe.Value, subxe.Attribute("margin").Value, subxe.Attribute("height").Value);
                            elementsubitems.Add(tempsubitem);
                        }
                    }
                    tempreq = new Req(xe.Attribute("id").Value, xe.Attribute("desc").Value, tempstatus, xe.Attribute("margin").Value, xe.Attribute("height").Value, elementsubitems);
                    reqlist.Add(tempreq);
                }
                else
                {
                    tempreq = new Req(xe.Attribute("id").Value, xe.Attribute("desc").Value, tempstatus, xe.Attribute("margin").Value, xe.Attribute("height").Value);
                    reqlist.Add(tempreq);
                }
            }
            return reqlist;
        }

        private void setRequirements()
        {
            //replace the 1st screeen already existing
            //int[] marginarray = new int[4];
            PivotItem firstpivot = new PivotItem();
            firstpivot.Header = requirementlist[0].RequirementId;
            TextBlock firstblock = new TextBlock();
            firstblock.Text = requirementlist[0].RequirementText;
            firstblock.Name = requirementlist[0].RequirementId;
            firstblock.Height = requirementlist[0].descriptionheight;
            firstblock.HorizontalAlignment = HorizontalAlignment.Left;
            //firstblock.Margin = new Thickness(14,163,0,0);
            firstblock.Margin = requirementlist[0].getMargin();
            firstblock.VerticalAlignment = VerticalAlignment.Top;
            firstblock.Width = 450;
            firstblock.TextWrapping = TextWrapping.Wrap;
            Grid firstgrid = new Grid();
            firstgrid.Children.Add(firstblock);
            if (requirementlist[0].hasSubItem())
            {
                //x
                List<SubItem> subitemvalues = requirementlist[0].getSubItemList();
                //ScrollViewer subitemviewer = new ScrollViewer();
                //StackPanel subitempanel = new StackPanel();
                for (int j = 0; j < subitemvalues.Count; j++)
                {
                    //x
                    if (subitemvalues[j].typestring.Equals("check"))
                    {
                        CheckBox tempcheck = new CheckBox();
                        tempcheck.Content = subitemvalues[j].descstring;
                        tempcheck.Name = subitemvalues[j].subitemid;
                        tempcheck.Height = subitemvalues[j].descriptionheight;
                        tempcheck.HorizontalAlignment = HorizontalAlignment.Left;
                        //firstblock.Margin = new Thickness(14,163,0,0);
                        tempcheck.Margin = subitemvalues[j].getMargin();
                        tempcheck.VerticalAlignment = VerticalAlignment.Top;
                        tempcheck.Width = 300;
                        if (subitemvalues[j].isSelected == true)
                        {
                            tempcheck.IsChecked = true;
                        }
                        firstgrid.Children.Add(tempcheck);
                        //subitempanel.Children.Add(tempcheck);
                    }
                }
                //subitemviewer.Content = subitempanel;
                //firstgrid.Children.Add(subitemviewer);
            }
            firstpivot.Content = firstgrid;
            firstclasspivot.Items[0] = firstpivot;
            //loop for remaining items
            for (int i = 1; i < requirementlist.Count; i++)
            {
                PivotItem temppivot = new PivotItem();
                temppivot.Header = requirementlist[i].RequirementId;
                TextBlock tempblock = new TextBlock();
                tempblock.Text = requirementlist[i].RequirementText;
                tempblock.Name = requirementlist[i].RequirementId;
                tempblock.Height = requirementlist[i].descriptionheight;
                tempblock.HorizontalAlignment = HorizontalAlignment.Left;
                //tempblock.Margin = new Thickness(14, 163, 0, 0);
                tempblock.Margin = requirementlist[i].getMargin();
                tempblock.VerticalAlignment = VerticalAlignment.Top;
                tempblock.Width = 450;
                tempblock.TextWrapping = TextWrapping.Wrap;
                Grid tempgrid = new Grid();
                tempgrid.Children.Add(tempblock);
                if (requirementlist[i].hasSubItem())
                {
                    //x
                    List<SubItem> subitemvalues = requirementlist[i].getSubItemList();
                    for (int j = 0; j < subitemvalues.Count; j++)
                    {
                        //x
                        if (subitemvalues[j].typestring.Equals("check"))
                        {
                            CheckBox tempcheck = new CheckBox();
                            tempcheck.Content = subitemvalues[j].descstring;
                            tempcheck.Name = subitemvalues[j].subitemid;
                            tempcheck.Height = subitemvalues[j].descriptionheight;
                            tempcheck.HorizontalAlignment = HorizontalAlignment.Left;
                            //firstblock.Margin = new Thickness(14,163,0,0);
                            tempcheck.Margin = subitemvalues[j].getMargin();
                            tempcheck.VerticalAlignment = VerticalAlignment.Top;
                            tempcheck.Width = subitemvalues[j].descriptionwidth;
                            if (subitemvalues[j].isSelected == true)
                            {
                                tempcheck.IsChecked = true;
                            }
                            tempgrid.Children.Add(tempcheck);
                        }
                        else if (subitemvalues[j].typestring.Equals("label"))
                        {
                            //x
                            TextBlock continueblock = new TextBlock();
                            continueblock.Text = subitemvalues[j].descstring;
                            continueblock.Name = subitemvalues[j].subitemid;
                            continueblock.Height = subitemvalues[j].descriptionheight;
                            continueblock.HorizontalAlignment = HorizontalAlignment.Left;
                            //firstblock.Margin = new Thickness(14,163,0,0);
                            continueblock.Margin = subitemvalues[j].getMargin();
                            continueblock.VerticalAlignment = VerticalAlignment.Top;
                            continueblock.Width = 300;
                            tempgrid.Children.Add(continueblock);

                        }
                        else if (subitemvalues[j].typestring.Equals("textentry"))
                        {
                            //x
                            TextBox entrybox = new TextBox();
                            TextBlock entryheader = new TextBlock();
                            entrybox.Name = subitemvalues[j].subitemid;
                            entryheader.Text = subitemvalues[j].descstring;
                            entryheader.Height = subitemvalues[j].descriptionheight;
                            entrybox.Height = subitemvalues[j].descriptionheight;
                            entryheader.Width = 300;
                            entrybox.Width = 300;
                            entryheader.Margin = subitemvalues[j].getMargin();
                            entrybox.Margin = new Thickness(entryheader.Margin.Left, (entryheader.Margin.Top + 20), entryheader.Margin.Right, entryheader.Margin.Bottom);
                            if (!subitemvalues[j].input.Equals(" "))
                            {
                                entrybox.Text = subitemvalues[j].input;
                            }
                            entryheader.HorizontalAlignment = HorizontalAlignment.Left;
                            entrybox.HorizontalAlignment = HorizontalAlignment.Left;
                            entryheader.VerticalAlignment = VerticalAlignment.Top;
                            entrybox.VerticalAlignment = VerticalAlignment.Top;
                            tempgrid.Children.Add(entryheader);
                            tempgrid.Children.Add(entrybox);

                        }
                    }
                }
                temppivot.Content = tempgrid;
                firstclasspivot.Items.Add(temppivot);
            }

            //req1.Header = requirementlist[0].RequirementId;
            //listtest.Text = requirementlist[0].RequirementText;
        }

        protected override void OnNavigatedFrom(System.Windows.Navigation.NavigationEventArgs e)
        {
            saveState();
        }

        public void saveState()
        {
            using (IsolatedStorageFile appstore = IsolatedStorageFile.GetUserStoreForApplication())
            {
                //using (IsolatedStorageFileStream xmlstream = appstore.CreateFile("scout_settings.xml"))
                using (IsolatedStorageFileStream xmlstream = new IsolatedStorageFileStream("scout_settings.xml", System.IO.FileMode.Create, System.IO.FileAccess.ReadWrite, appstore))
                {
                    foreach (XElement xe in loadeddata.Element("Scout").Element("Ranks").Element("FirstClass").Element("Requirements").Nodes())
                    {
                        //savewriter.WriteStartDocument();
                        if (xe.HasElements == true)
                        {
                            for (int a = 0; a < firstclasspivot.Items.Count; a++)
                            {
                                PivotItem savepivot = (PivotItem)firstclasspivot.Items[a];
                                if (savepivot.Header.Equals(xe.Attribute("id").Value))
                                {
                                    Grid savegrid = (Grid)savepivot.Content;
                                    int b = 0;
                                    foreach (XElement subxe in xe.Nodes())
                                    {
                                        //x
                                        if (subxe.Name.LocalName.Equals("check"))
                                        {
                                            //x
                                            bool selected = false;
                                            while (b < savegrid.Children.Count)
                                            {
                                                if (savegrid.Children[b] is CheckBox)
                                                {
                                                    CheckBox savecheckbox = (CheckBox)savegrid.Children[b];
                                                    if (savecheckbox.IsChecked == true)
                                                    {
                                                        selected = true;
                                                    }
                                                    if (selected == true)
                                                    {
                                                        subxe.Attribute("checked").Value = "true";
                                                    }
                                                    else
                                                    {
                                                        subxe.Attribute("checked").Value = "false";
                                                    }
                                                    b++;
                                                    break;
                                                }
                                                b++;
                                            }
                                        }
                                        else if (subxe.Name.LocalName.Equals("textentry"))
                                        {
                                            while (b < savegrid.Children.Count)
                                            {
                                                if (savegrid.Children[b] is TextBox)
                                                {
                                                    TextBox savetextbox = (TextBox)savegrid.Children[b];
                                                    if (!(savetextbox.Text.Equals(" ") || savetextbox.Text.Equals("")))
                                                    {
                                                        subxe.Attribute("entry").Value = savetextbox.Text;
                                                    }
                                                    b++;
                                                    break;
                                                }
                                                b++;
                                            }
                                        }
                                    }
                                    a = firstclasspivot.Items.Count - 1;
                                }
                            }
                        }
                    }
                    loadeddata.Save(xmlstream);
                }
            }
        }
    }
}