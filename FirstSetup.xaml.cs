﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Microsoft.Phone.Controls;
using System.Xml.Linq;
using System.Windows.Navigation;
using System.IO.IsolatedStorage;

namespace BSA_Eagle_Tracker
{
    public partial class FirstSetup : PhoneApplicationPage
    {
        private List<string> religionselection;
        private string birthdaystring;
        private DateTime prevtime;
        private string[] religionchoices = { "Catholic", "Protestant", "Judaism" };
        //private Uri backuri;

        public FirstSetup()
        {
            InitializeComponent();
            religionselection = new List<string>();
            religionselection.Add(" ");
            /*
            religionselection.Add("Catholic");
            //religionselection.Add("Roman Catholic");
            //religionselection.Add("Eastern Catholic");
            religionselection.Add("Protestant");
            religionselection.Add("Judaism");
             */
            religionselection.AddRange(religionchoices.ToList());
            this.DataContext = this;
            religionPicker.ItemsSource = religionselection;
            //religionPicker.ItemCountThreshold = 2;
            prevtime = DateTime.Now;
        }

        /*
        protected override void OnNavigatedFrom(System.Windows.Navigation.NavigationEventArgs e) {
            //backuri = new Uri("/MainPage.xaml");
            if (!StorageHelper.fileInIS("scout_settings.xml"))
            {
                NavigationEventArgs tempnav = new NavigationEventArgs(e.Content, new Uri("/MainPage.xaml?data=false", UriKind.Relative));
                NavigationContext.QueryString = te
                //e.Uri = new Uri("/MainPage.xaml?data=false");
                e = tempnav;
            }
        }
         */

        private void submitbutton_Click(object sender, RoutedEventArgs e)
        {
            if (birthdaystring != null)
            {
                saveInformation();
                NavigationService.GoBack();
            }
            else
            {
                MessageBox.Show("You MUST enter a valid birthday within boy scout age range (ages 11-18)");
            }
        }

        private void birthdatePicker_ValueChanged(object sender, DateTimeValueChangedEventArgs e)
        {
            DateTime birthday = (DateTime)birthdatePicker.Value;
            //check year
            if (birthday.Year < (DateTime.Now.Year - 18))
            {
                //too old to be a boy scout
                birthdatePicker.ValueChanged -= new EventHandler<DateTimeValueChangedEventArgs>(birthdatePicker_ValueChanged);
                birthdatePicker.Value = prevtime;
                birthdatePicker.ValueChanged += new EventHandler<DateTimeValueChangedEventArgs>(birthdatePicker_ValueChanged);
                MessageBox.Show("Too Old");
            }
            else if (birthday.Year > (DateTime.Now.Year - 11))
            {
                //too young to be a boy scout
                birthdatePicker.ValueChanged -= new EventHandler<DateTimeValueChangedEventArgs>(birthdatePicker_ValueChanged);
                birthdatePicker.Value = prevtime;
                birthdatePicker.ValueChanged += new EventHandler<DateTimeValueChangedEventArgs>(birthdatePicker_ValueChanged);
                MessageBox.Show("Too Young");
            }
            else
            {
                //check month
                if (birthday.Month < DateTime.Now.Month)
                {
                    //too old to be a boy scout
                    birthdatePicker.ValueChanged -= new EventHandler<DateTimeValueChangedEventArgs>(birthdatePicker_ValueChanged);
                    birthdatePicker.Value = prevtime;
                    birthdatePicker.ValueChanged += new EventHandler<DateTimeValueChangedEventArgs>(birthdatePicker_ValueChanged);
                    MessageBox.Show("Too Old");
                }
                else if (birthday.Month > DateTime.Now.Month)
                {
                    //too young to be a boy scout
                    birthdatePicker.ValueChanged -= new EventHandler<DateTimeValueChangedEventArgs>(birthdatePicker_ValueChanged);
                    birthdatePicker.Value = prevtime;
                    birthdatePicker.ValueChanged += new EventHandler<DateTimeValueChangedEventArgs>(birthdatePicker_ValueChanged);
                    MessageBox.Show("Too Young");
                }
                else
                {
                    //check day
                    if (birthday.Day < DateTime.Now.Day)
                    {
                        //too old to be a boy scout
                        birthdatePicker.ValueChanged -= new EventHandler<DateTimeValueChangedEventArgs>(birthdatePicker_ValueChanged);
                        birthdatePicker.Value = prevtime;
                        birthdatePicker.ValueChanged += new EventHandler<DateTimeValueChangedEventArgs>(birthdatePicker_ValueChanged);
                        MessageBox.Show("Too Old");
                    }
                    else if (birthday.Day > DateTime.Now.Day)
                    {
                        //too young to be a boy scout
                        birthdatePicker.ValueChanged -= new EventHandler<DateTimeValueChangedEventArgs>(birthdatePicker_ValueChanged);
                        birthdatePicker.Value = prevtime;
                        birthdatePicker.ValueChanged += new EventHandler<DateTimeValueChangedEventArgs>(birthdatePicker_ValueChanged);
                        MessageBox.Show("Too Young");
                    }
                    else
                    {
                        birthdaystring = birthday.ToShortDateString();
                    }
                }
            }
        }

        public void saveInformation()
        {
            XDocument setdoc = XDocument.Load("scout_data.xml");
            using (IsolatedStorageFile tempsavefile = IsolatedStorageFile.GetUserStoreForApplication())
            {
                //IsolatedStorageFileStream settingstream = new IsolatedStorageFileStream("scout_settings.xml", System.IO.FileMode.CreateNew, tempsavefile);
                foreach (XElement xe in setdoc.Element("Scout").Element("information").Nodes())
                {
                    //x
                    if (xe.Name.LocalName.Equals("name"))
                    {
                        //x
                        xe.Value = nameBox.Text;
                    }
                    else if (xe.Name.LocalName.Equals("birthday"))
                    {
                        xe.Value = birthdaystring;
                    }
                    else if (xe.Name.LocalName.Equals("religion"))
                    {
                        //x
                        xe.Value = (string)religionPicker.SelectedItem;
                    }
                }
                using (IsolatedStorageFileStream settingsstream = tempsavefile.CreateFile("scout_settings.xml"))
                {
                    setdoc.Save(settingsstream);
                    settingsstream.Close();
                    settingsstream.Dispose();
                }
                //setdoc.Save(settingstream);
                //settingstream.Close();
                //StorageHelper.saveSettingsXml("scout_settings.xml", setdoc);
                tempsavefile.Dispose();
            }
            setdoc = null;
        }
    }
}