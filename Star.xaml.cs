﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Microsoft.Phone.Controls;
using System.ComponentModel;
using System.Xml.Linq;
using System.Xml;
using System.IO.IsolatedStorage;
using System.IO;

namespace BSA_Eagle_Tracker
{
    public partial class Star : PhoneApplicationPage
    {
        private List<Req> requirementlist;
        private List<MeritBadge> eaglebadgelist;
        private bool[] badgetaken;
        private string[] previousmbname;
        public ListPicker[] meritbadgeselectors;
        private static XDocument loadeddata;
        //private int[] previousselection;

        public Star()
        {
            InitializeComponent();
            loadeddata = StorageHelper.getSettingsXml("scout_settings.xml");
            requirementlist = getRequirementList();
            meritbadgeselectors = new ListPicker[4];
            previousmbname = new string[4];
            for (int i = 0; i < meritbadgeselectors.Length; i++)
            {
                //x
                meritbadgeselectors[i] = new ListPicker();
                previousmbname[i] = " ";
            }
            eaglebadgelist = setMeritBadgeList();
            badgetaken = new bool[eaglebadgelist.Count];
            this.DataContext = this;
            setRequirements();
        }

        public static List<Req> getRequirementList()
        {
            //x
            List<Req> reqlist = new List<Req>();
            //XDocument loadeddata = StorageHelper.getSettingsXml("scout_settings.xml");
            /*
            var data = from query in loadeddata.Element("Tenderfoot").Descendants()
                       select new Req
                       {
                           reqid = (string)query.Attribute("id"),
                           RequirementText = (string)query.Element("req"),
                           RequirementFinished = bool.Parse(query.Attribute("used").Value)
                           
                       };
            */
            var data = from query in loadeddata.Element("Scout").Element("Ranks").Element("Star").Element("Requirements").Nodes()
                       select query;

            foreach (XElement xe in data)
            {
                Req tempreq;
                bool tempstatus = bool.Parse(xe.Attribute("done").Value);
                /*
                if (xe.HasElements == true && xe.Name.LocalName.Equals("dropdown"))
                {
                    //x
                    List<SubItem> elementsubitems = new List<SubItem>();
                    foreach (XElement subxe in xe.Element("dropdown").Nodes())
                    {
                        SubItem tempsubitem = new SubItem(subxe.Name.LocalName, subxe.Value, subxe.Attribute("margin").Value, subxe.Attribute("height").Value);
                        elementsubitems.Add(tempsubitem);
                    }
                }
                 */
                if (xe.HasElements == true)
                {
                    //x
                    List<SubItem> elementsubitems = new List<SubItem>();
                    foreach (XElement subxe in xe.Nodes())
                    {
                        //x
                        if (subxe.Name.LocalName.Equals("dropdown"))
                        {
                            //x
                            foreach (XElement dropdownxe in subxe.Nodes())
                            {
                                //x
                                bool dropdownchosen = bool.Parse(dropdownxe.Attribute("selected").Value);
                                if (dropdownchosen == true)
                                {
                                    SubItem tempsubitem = new SubItem(dropdownxe.Name.LocalName, xe.Attribute("id").Value, dropdownxe.Value, dropdownxe.Attribute("margin").Value, dropdownxe.Attribute("height").Value, dropdownchosen);
                                    elementsubitems.Add(tempsubitem);
                                }
                                else
                                {
                                    SubItem tempsubitem = new SubItem(dropdownxe.Name.LocalName, xe.Attribute("id").Value, dropdownxe.Value, dropdownxe.Attribute("margin").Value, dropdownxe.Attribute("height").Value);
                                    elementsubitems.Add(tempsubitem);
                                }
                            }
                        }
                        else
                        {
                            SubItem tempsubitem = new SubItem(subxe.Name.LocalName, xe.Attribute("id").Value, subxe.Value, subxe.Attribute("margin").Value, subxe.Attribute("height").Value);
                            elementsubitems.Add(tempsubitem);
                        }
                    }
                    tempreq = new Req(xe.Attribute("id").Value, xe.Attribute("desc").Value, tempstatus, xe.Attribute("margin").Value, xe.Attribute("height").Value, elementsubitems);
                    reqlist.Add(tempreq);
                }
                else
                {
                    tempreq = new Req(xe.Attribute("id").Value, xe.Attribute("desc").Value, tempstatus, xe.Attribute("margin").Value, xe.Attribute("height").Value);
                    reqlist.Add(tempreq);
                }
            }
            return reqlist;
        }

        private void setRequirements()
        {
            //replace the 1st screeen already existing
            PivotItem firstpivot = new PivotItem();
            firstpivot.Header = requirementlist[0].RequirementId;
            TextBlock firstblock = new TextBlock();
            firstblock.Text = requirementlist[0].RequirementText;
            firstblock.Height = requirementlist[0].descriptionheight;
            firstblock.HorizontalAlignment = HorizontalAlignment.Left;
            //firstblock.Margin = new Thickness(14,163,0,0);
            firstblock.Margin = requirementlist[0].getMargin();
            firstblock.VerticalAlignment = VerticalAlignment.Top;
            firstblock.Width = 450;
            firstblock.TextWrapping = TextWrapping.Wrap;
            Grid firstgrid = new Grid();
            firstgrid.Children.Add(firstblock);
            if (requirementlist[0].hasSubItem())
            {
                //x
                List<SubItem> subitemvalues = requirementlist[0].getSubItemList();
                //ScrollViewer subitemviewer = new ScrollViewer();
                //StackPanel subitempanel = new StackPanel();
                for (int j = 0; j < subitemvalues.Count; j++)
                {
                    //x
                    if (subitemvalues[j].typestring.Equals("check"))
                    {
                        CheckBox tempcheck = new CheckBox();
                        tempcheck.Content = subitemvalues[j].descstring;
                        tempcheck.Name = subitemvalues[j].subitemid;
                        tempcheck.Height = subitemvalues[j].descriptionheight;
                        tempcheck.HorizontalAlignment = HorizontalAlignment.Left;
                        //firstblock.Margin = new Thickness(14,163,0,0);
                        tempcheck.Margin = subitemvalues[j].getMargin();
                        tempcheck.VerticalAlignment = VerticalAlignment.Top;
                        tempcheck.Width = subitemvalues[j].descriptionwidth;
                        firstgrid.Children.Add(tempcheck);
                        //subitempanel.Children.Add(tempcheck);
                    }
                    else if (subitemvalues[j].typestring.Equals("dropdownitem"))
                    {
                        //this.DataContext = this;
                        //List<string> dropdownlistitems = new List<string>();
                        List<string> subitemchoices = new List<string>();
                        ListPicker choicelist = new ListPicker();
                        //dropdownlistitems.Add(subitemvalues[j]);
                        int dropdowncounter = j;
                        //dropdowncounter++;
                        while ((dropdowncounter < (subitemvalues.Count)) && (subitemvalues[dropdowncounter].typestring.Equals("dropdownitem")))
                        {
                            //ListPickerItem tempitem = new ListPickerItem();
                            //tempitem.Content = subitemvalues[dropdowncounter].descstring;
                            //tempitem.Height = subitemvalues[dropdowncounter].descriptionheight;
                            //tempitem.Width = 300;
                            subitemchoices.Add(subitemvalues[dropdowncounter].descstring);
                            dropdowncounter++;
                        }
                        choicelist.ItemsSource = subitemchoices;
                        choicelist.Margin = subitemvalues[j].getMargin();
                        firstgrid.Children.Add(choicelist);
                        j = dropdowncounter;
                    }
                }
                //subitemviewer.Content = subitempanel;
                //firstgrid.Children.Add(subitemviewer);
            }
            firstpivot.Content = firstgrid;
            starpivot.Items[0] = firstpivot;
            //loop for remaining items
            for (int i = 1; i < requirementlist.Count; i++)
            {
                PivotItem temppivot = new PivotItem();
                temppivot.Header = requirementlist[i].RequirementId;
                TextBlock tempblock = new TextBlock();
                tempblock.Text = requirementlist[i].RequirementText;
                tempblock.Height = requirementlist[i].descriptionheight;
                tempblock.HorizontalAlignment = HorizontalAlignment.Left;
                //tempblock.Margin = new Thickness(14, 163, 0, 0);
                tempblock.Margin = requirementlist[i].getMargin();
                tempblock.VerticalAlignment = VerticalAlignment.Top;
                tempblock.Width = 450;
                tempblock.TextWrapping = TextWrapping.Wrap;
                Grid tempgrid = new Grid();
                tempgrid.Children.Add(tempblock);

                //handling for merit badges
                if (requirementlist[i].RequirementId.Equals("3")) {
                    List<SubItem> mbitemvalues = requirementlist[i].getSubItemList();
                    //meritbadgeselectors = new ListPicker[4];
                    List<string> firstmblist = new List<string>();
                    //firstmblist.Add("Select Merit Badge");
                    firstmblist.AddRange(getMeritBadgeList());
                    //previousselection = new int[meritbadgeselectors.Length];
                    
                    for (int j = 0; j < meritbadgeselectors.Length; j++)
                    {
                        /*
                        for (int b = 0; b < eaglebadgelist.Count; b++)
                        {
                            //x
                            if (badgetaken[b] == false)
                            {
                                firstmblist.Add(eaglebadgelist[b].badgeName);
                                if (j == 0 || previousselection[j] != j)
                                {
                                    //x
                                    badgetaken[j] = true;
                                    previousselection[j] = j;
                                }
                            }
                        }
                         */
                        meritbadgeselectors[j].ItemCountThreshold = 3;
                        meritbadgeselectors[j].ItemsSource = firstmblist;
                        if (!previousmbname[j].Equals(" "))
                        {
                            if (previousmbname[j].Equals("Emergency Preparedness") || previousmbname[j].Equals("Lifesaving"))
                            {
                                //x
                                for (int ij = 0; ij < meritbadgeselectors.Length; ij++)
                                {
                                    if (ij != j)
                                    {
                                        List<string> optionmblist = new List<string>();
                                        optionmblist.AddRange(getMeritBadgeList1(previousmbname[j]));
                                        meritbadgeselectors[ij].ItemsSource = optionmblist;
                                        if (optionmblist.Contains(previousmbname[ij]))
                                        {
                                            meritbadgeselectors[ij].SelectedIndex = optionmblist.IndexOf(previousmbname[ij]);
                                        }
                                        else
                                        {
                                            meritbadgeselectors[ij].SelectedIndex = 0;
                                        }
                                    }
                                    meritbadgeselectors[j].SelectedIndex = firstmblist.IndexOf(previousmbname[j]);
                                }
                            }
                            else if (previousmbname[j].Equals("Swimming") || previousmbname[j].Equals("Hiking") || previousmbname[j].Equals("Cycling"))
                            {
                                //x
                                
                                for (int ij = 0; ij < meritbadgeselectors.Length; ij++)
                                {
                                    if (ij != j)
                                    {
                                        List<string> optionmblist = new List<string>();
                                        optionmblist.AddRange(getMeritBadgeList2(previousmbname[j]));
                                        meritbadgeselectors[ij].ItemsSource = optionmblist;
                                        if (optionmblist.Contains(previousmbname[ij]))
                                        {
                                            meritbadgeselectors[ij].SelectedIndex = optionmblist.IndexOf(previousmbname[ij]);
                                        }
                                        else
                                        {
                                            meritbadgeselectors[ij].SelectedIndex = 0;
                                        }
                                    }
                                }
                                meritbadgeselectors[j].SelectedIndex = firstmblist.IndexOf(previousmbname[j]);
                                
                            }
                            else
                            {
                                meritbadgeselectors[j].SelectedIndex = firstmblist.IndexOf(previousmbname[j]);
                            }
                        }
                        //meritbadgeselectors[j].Name = mbitemvalues[j].descstring;
                        meritbadgeselectors[j].Margin = mbitemvalues[j].getMargin();
                        //meritbadgeselectors[j].Height = mbitemvalues[j].descriptionheight;
                        meritbadgeselectors[j].Width = 350;
                        meritbadgeselectors[j].HorizontalAlignment = HorizontalAlignment.Left;
                        meritbadgeselectors[j].VerticalAlignment = VerticalAlignment.Top;
                        if (j == 0)
                        {
                            meritbadgeselectors[j].Header = "Required";
                        }
                        //meritbadgeselectors[j].DataContext = this;
                        //meritbadgeselectors[j].ItemsSource = getMeritBadgeList();
                        //meritbadgeselectors[j].ListPickerMode = ListPickerMode.Full;
                        //meritbadgeselectors[j].SelectedIndex = 0;
                        tempgrid.Children.Add(meritbadgeselectors[j]);
                        //if (j == 0) {
                        meritbadgeselectors[j].SelectionChanged += new SelectionChangedEventHandler(meritBadgeSelectors_SelectionChanged);
                        //}
                    }
                    //this.DataContext = this;
                }
                    //everything else (other than merit badges
                else if (requirementlist[i].hasSubItem())
                {
                    //x
                    List<SubItem> subitemvalues = requirementlist[i].getSubItemList();
                    for (int j = 0; j < subitemvalues.Count; j++)
                    {
                        //x
                        if (subitemvalues[j].typestring.Equals("check"))
                        {
                            CheckBox tempcheck = new CheckBox();
                            tempcheck.Content = subitemvalues[j].descstring;
                            tempcheck.Height = subitemvalues[j].descriptionheight;
                            tempcheck.HorizontalAlignment = HorizontalAlignment.Left;
                            //firstblock.Margin = new Thickness(14,163,0,0);
                            tempcheck.Margin = subitemvalues[j].getMargin();
                            tempcheck.VerticalAlignment = VerticalAlignment.Top;
                            tempcheck.Width = 300;
                            tempgrid.Children.Add(tempcheck);
                        }
                        else if (subitemvalues[j].typestring.Equals("label"))
                        {
                            //x
                            TextBlock continueblock = new TextBlock();
                            continueblock.Text = subitemvalues[j].descstring;
                            continueblock.Height = subitemvalues[j].descriptionheight;
                            continueblock.HorizontalAlignment = HorizontalAlignment.Left;
                            //firstblock.Margin = new Thickness(14,163,0,0);
                            continueblock.Margin = subitemvalues[j].getMargin();
                            continueblock.VerticalAlignment = VerticalAlignment.Top;
                            continueblock.Width = 300;
                            continueblock.FontSize = 22;
                            continueblock.TextWrapping = TextWrapping.Wrap;
                            tempgrid.Children.Add(continueblock);
                        }
                        else if (subitemvalues[j].typestring.Equals("dropdownitem"))
                        {
                            
                            //List<string> dropdownlistitems = new List<string>();
                            List<string> subitemchoices = new List<string>();
                            ListPicker choicelist = new ListPicker();
                            //dropdownlistitems.Add(subitemvalues[j]);
                            int dropdowncounter = j;
                            string chosendropdown = " ";
                            //dropdowncounter++;
                            while ((dropdowncounter < (subitemvalues.Count)) && (subitemvalues[dropdowncounter].typestring.Equals("dropdownitem")))
                            {
                                if (subitemvalues[dropdowncounter].isSelected == true)
                                {
                                    chosendropdown = subitemvalues[dropdowncounter].descstring;
                                }
                                subitemchoices.Add(subitemvalues[dropdowncounter].descstring);
                                //if (dropdowncounter < (subitemvalues.Count - 1))
                                //{
                                    dropdowncounter++;
                                //}
                            }
                            choicelist.ItemsSource = subitemchoices;
                            choicelist.Margin = subitemvalues[j].getMargin();
                            if (!chosendropdown.Equals(" "))
                            {
                                choicelist.SelectedIndex = subitemchoices.IndexOf(chosendropdown);
                            }
                            //choicelist.SelectedIndex = subitemchoices.IndexOf(chosendropdown);
                            tempgrid.Children.Add(choicelist);
                            //this.DataContext = this;
                            //choicelist.DataContext = this;
                            j = dropdowncounter;
                        }
                    }
                }
                temppivot.Content = tempgrid;
                starpivot.Items.Add(temppivot);
            }

            //req1.Header = requirementlist[0].RequirementId;
            //listtest.Text = requirementlist[0].RequirementText;
        }

        private void meritBadgeSelectors_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            //x
            //ListPicker temppickeritem = (ListPicker)sender;
            //firstmblist.Add("Select Merit Badge");
            string selecteddata = (sender as ListPicker).SelectedItem as string;
            int previousselectionpos = 0;
            ListPicker tempchoose = (ListPicker)sender;
            for (int ba = 0; ba < meritbadgeselectors.Length; ba++)
            {
                //x
                if (tempchoose.Equals(meritbadgeselectors[ba]))
                {
                    previousselectionpos = ba;
                }
            }
            if (tempchoose.SelectedItem.Equals("Select Merit Badge"))
            {
                //x
                for (int i = 0; i < eaglebadgelist.Count; i++)
                {
                    //x
                    if (eaglebadgelist[i].badgeName.Equals(previousmbname[previousselectionpos])) {
                        eaglebadgelist[i].recieved = false;
                    }
                }
                previousmbname[previousselectionpos] = " ";
                for (int j = 0; j < meritbadgeselectors.Length; j++) {
                    //x
                    //if (j != previousselectionpos) {
                        
                    //}
                    List<string> tempmeritbadgelist = getMeritBadgeList();
                    for (int dc = 0; dc < previousmbname.Length; dc++)
                        {
                            //x
                            if (j != dc)
                            {
                                tempmeritbadgelist.Remove(previousmbname[dc]);
                            }
                        }
                    meritbadgeselectors[j].ItemsSource = tempmeritbadgelist;
                    if (j != previousselectionpos && !previousmbname[j].Equals(" "))
                    {
                        //x
                        meritbadgeselectors[j].SelectedIndex = meritbadgeselectors[j].Items.IndexOf(previousmbname[j]);
                    }
                }
            }
            else
            {
                for (int i = 0; i < eaglebadgelist.Count; i++)
                {
                    if (selecteddata.Equals(eaglebadgelist[i].badgeName))
                    {
                        eaglebadgelist[i].recieved = true;
                    }
                }
                if (!previousmbname[previousselectionpos].Equals(" ")) {
                    //x
                    for (int j = 0; j < eaglebadgelist.Count; j++) {
                        //x
                        if (eaglebadgelist[j].badgeName.Equals(previousmbname[previousselectionpos])) {
                            eaglebadgelist[j].recieved = false;
                        }
                    }
                }
                previousmbname[previousselectionpos] = selecteddata;
                if (selecteddata.Equals("Emergency Preparedness") || selecteddata.Equals("Lifesaving") || selecteddata.Equals("Swimming") || selecteddata.Equals("Hiking") || selecteddata.Equals("Cycling"))
                {
                    //x
                    for (int i = 0; i < meritbadgeselectors.Length; i++)
                    {
                        if (i != previousselectionpos)
                        {
                            List<string> tempmeritbadgelist = null;
                            if (selecteddata.Equals("Emergency Preparedness") || selecteddata.Equals("Lifesaving"))
                            {
                                //x
                                tempmeritbadgelist = getMeritBadgeList1(selecteddata);
                            }
                            else if (selecteddata.Equals("Swimming") || selecteddata.Equals("Hiking") || selecteddata.Equals("Cycling"))
                            {
                                tempmeritbadgelist = getMeritBadgeList2(selecteddata);
                            }
                            for (int dc = 0; dc < previousmbname.Length; dc++)
                            {
                                //x
                                if (i != dc)
                                {
                                    tempmeritbadgelist.Remove(previousmbname[dc]);
                                }
                            }
                            meritbadgeselectors[i].ItemsSource = tempmeritbadgelist;
                        }
                    }
                }
                else
                {
                    for (int i = 0; i < meritbadgeselectors.Length; i++)
                    {
                        //x
                        if (i != previousselectionpos)
                        {
                            List<string> tempmeritbadgelist = getMeritBadgeList();
                            for (int dc = 0; dc < previousmbname.Length; dc++)
                            {
                                //x
                                if (i != dc)
                                {
                                    tempmeritbadgelist.Remove(previousmbname[dc]);
                                }
                            }
                            meritbadgeselectors[i].ItemsSource = tempmeritbadgelist;
                        }
                    }
                }
            }
        }

        private List<string> getMeritBadgeList()
        {
            List<string> tempmblist = new List<string>();
            tempmblist.Add("Select Merit Badge");
            //int badgeuse = 0;
            for (int i = 0; i < eaglebadgelist.Count; i++)
            {
                for (int p = 0; p < meritbadgeselectors.Length; p++)
                {
                    if (eaglebadgelist[i].recieved == false || previousmbname[p].Equals(eaglebadgelist[i].badgeName))
                    {
                        if (!tempmblist.Contains(eaglebadgelist[i].badgeName))
                        {
                            tempmblist.Add(eaglebadgelist[i].badgeName);
                            //badgetaken[badgeuse] = true;
                        }
                    }
                }
            }
            return tempmblist;
        }

        private List<string> getMeritBadgeList1(string groupbadge1)
        {
            //List<string> tempmblist = new List<string>();
            List<string> tempmblist = getMeritBadgeList();
            //tempmblist.Add("Select Merit Badge");
            //int badgeuse = 0;
            //for (int i = 0; i < eaglebadgelist.Count; i++)
            //{
                //for (int p = 0; p < meritbadgeselectors.Length; p++)
                //{
                    //if (eaglebadgelist[i].recieved == false || previousmbname[p].Equals(eaglebadgelist[i].badgeName))
                    //{
                        if (groupbadge1.Equals("Emergency Preparedness")) {
                            /*
                            if (!eaglebadgelist[i].badgeName.Equals("Lifesaving") && !tempmblist.Contains(eaglebadgelist[i].badgeName))
                            {
                                tempmblist.Add(eaglebadgelist[i].badgeName);
                                //badgetaken[badgeuse] = true;
                            }
                             */
                            tempmblist.Remove("Lifesaving");
                        }
                        else if (groupbadge1.Equals("Lifesaving"))
                        {
                            /*
                            if (!eaglebadgelist[i].badgeName.Equals("Emergency Preparedness") && !tempmblist.Contains(eaglebadgelist[i].badgeName))
                            {
                                tempmblist.Add(eaglebadgelist[i].badgeName);
                                //badgetaken[badgeuse] = true;
                            }
                             */
                            tempmblist.Remove("Emergency Preparedness");
                        }
                    //}
                //}
            //}
            return tempmblist;
        }

        private List<string> getMeritBadgeList2(string groupbadge2)
        {
            //List<string> tempmblist = new List<string>();
            //List<string> tempmblist = getMeritBadgeList();
            List<string> tempmblist;
            if (previousmbname.Contains("Emergency Preparedness"))
            {
                tempmblist = getMeritBadgeList1("Emergency Preparedness");
            }
            else if (previousmbname.Contains("Lifesaving"))
            {
                tempmblist = getMeritBadgeList1("Lifesaving");
            }
            else
            {
                tempmblist = getMeritBadgeList();
            }
            //tempmblist.Add("Select Merit Badge");
            //int badgeuse = 0;
            /*
            for (int i = 0; i < eaglebadgelist.Count; i++)
            {
                for (int p = 0; p < meritbadgeselectors.Length; p++)
                {
             */
                    if (groupbadge2.Equals("Swimming"))
                    {
                        /*
                        if (!(eaglebadgelist[i].badgeName.Equals("Hiking") || eaglebadgelist[i].badgeName.Equals("Cycling")) && !tempmblist.Contains(eaglebadgelist[i].badgeName))
                        {
                            tempmblist.Add(eaglebadgelist[i].badgeName);
                            //badgetaken[badgeuse] = true;
                        }
                         */
                        tempmblist.Remove("Hiking");
                        tempmblist.Remove("Cycling");
                    }
                    else if (groupbadge2.Equals("Hiking"))
                    {
                        /*
                        if (!(eaglebadgelist[i].badgeName.Equals("Swimming") || eaglebadgelist[i].badgeName.Equals("Cycling")) && !tempmblist.Contains(eaglebadgelist[i].badgeName))
                        {
                            tempmblist.Add(eaglebadgelist[i].badgeName);
                            //badgetaken[badgeuse] = true;
                        }
                         */
                        tempmblist.Remove("Swimming");
                        tempmblist.Remove("Cycling");
                    }
                    else if (groupbadge2.Equals("Cycling"))
                    {
                        /*
                        if (!(eaglebadgelist[i].badgeName.Equals("Hiking") || eaglebadgelist[i].badgeName.Equals("Swimming")) && !tempmblist.Contains(eaglebadgelist[i].badgeName))
                        {
                            tempmblist.Add(eaglebadgelist[i].badgeName);
                            //badgetaken[badgeuse] = true;
                        }
                         */
                        tempmblist.Remove("Swimming");
                        tempmblist.Remove("Hiking");
                    }
            /*
                }
            }
             */
            return tempmblist;
        }

        private List<MeritBadge> setMeritBadgeList()
        {
            //x
            List<MeritBadge> eblist = new List<MeritBadge>();

            var mbdata = from query in loadeddata.Element("Scout").Element("MeritBadges").Nodes()
                       select query;

            int badgeuse = 0;
            int group1taken = -1;
            List<MeritBadge> group1names = new List<MeritBadge>();
            int group2taken = -1;
            List<MeritBadge> group2names = new List<MeritBadge>();

            foreach (XElement xe in mbdata)
            {
                //x
                bool iseaglerequired = bool.Parse(xe.Attribute("eaglerequired").Value);
                
                if ((!xe.Attribute("rank").Value.Equals("blank")) && (xe.Value.Equals("Emergency Preparedness") || xe.Value.Equals("Lifesaving")))
                {
                    if (xe.Value.Equals("Emergency Preparedness"))
                    {
                        group1taken = 1;
                    }
                    else if (xe.Value.Equals("Lifesaving")) {
                        group1taken = 2;
                    }
                    //group1 = true;
                }
                if ((!xe.Attribute("rank").Value.Equals("blank")) && (xe.Value.Equals("Swimming") || xe.Value.Equals("Hiking") || xe.Value.Equals("Cycling")))
                {
                    if (xe.Value.Equals("Swimming"))
                    {
                        group2taken = 1;
                    }
                    else if (xe.Value.Equals("Hiking")) {
                        group2taken = 2;
                    }
                    else if (xe.Value.Equals("Cycling")) {
                        group2taken = 3;
                    }
                    //group2 = true;
                }
                if (iseaglerequired  == true) {
                    if (xe.Attribute("rank").Value.Equals("star"))
                    {
                        MeritBadge tempbadge = new MeritBadge(xe.Value, true, bool.Parse(xe.Attribute("recieved").Value), xe.Attribute("rank").Value);
                        if (badgeuse < meritbadgeselectors.Length && (!previousmbname.Contains(xe.Value)))
                        {
                            previousmbname[badgeuse] = xe.Value;
                            badgeuse++;
                        }
                        eblist.Add(tempbadge);
                    }
                    else
                    {
                        if ((xe.Value.Equals("Emergency Preparedness") || xe.Value.Equals("Lifesaving")))
                        {
                            //x
                            MeritBadge tempbadge = new MeritBadge(xe.Value, true, bool.Parse(xe.Attribute("recieved").Value));
                            group1names.Add(tempbadge);
                            if (xe.Value.Equals("Lifesaving"))
                            {
                                //MeritBadge tempbadge = new MeritBadge(xe.Value, true, bool.Parse(xe.Attribute("recieved").Value));
                                //eblist.Add(tempbadge);
                                if (group1taken == -1)
                                {
                                    if (!(eblist.Contains(group1names[0]) || eblist.Contains(group1names[1])))
                                    {
                                        eblist.AddRange(group1names);
                                    }
                                }
                                else if (group1taken == 1)
                                {
                                    if (!eblist.Contains(group1names[0]))
                                    {
                                        eblist.Add(group1names[0]);
                                    }
                                }
                                else if (group1taken == 2)
                                {
                                    if (!eblist.Contains(group1names[1]))
                                    {
                                        eblist.Add(group1names[1]);
                                    }
                                }
                            }
                        }
                        else if (xe.Value.Equals("Swimming") || xe.Value.Equals("Hiking") || xe.Value.Equals("Cycling"))
                        {
                            //x
                            MeritBadge tempbadge = new MeritBadge(xe.Value, true, bool.Parse(xe.Attribute("recieved").Value));
                            group2names.Add(tempbadge);
                            if (xe.Value.Equals("Cycling"))
                            {
                                if (group2taken == -1)
                                {
                                    if (!(eblist.Contains(group2names[0]) || eblist.Contains(group2names[1]) || eblist.Contains(group2names[2])))
                                    {
                                        eblist.AddRange(group2names);
                                    }
                                }
                                else if (group2taken == 1)
                                {
                                    if (!eblist.Contains(group2names[0]))
                                    {
                                        eblist.Add(group2names[0]);
                                    }
                                }
                                else if (group1taken == 2)
                                {
                                    if (!eblist.Contains(group2names[1]))
                                    {
                                        eblist.Add(group2names[1]);
                                    }
                                }
                                else if (group1taken == 3)
                                {
                                    if (!eblist.Contains(group2names[2]))
                                    {
                                        eblist.Add(group2names[2]);
                                    }
                                }
                            }
                        }
                        else
                        {
                            MeritBadge tempbadge = new MeritBadge(xe.Value, true, bool.Parse(xe.Attribute("recieved").Value));
                            eblist.Add(tempbadge);
                        }
                    }
                }
            }
            return eblist;
        }

        protected override void OnNavigatedFrom(System.Windows.Navigation.NavigationEventArgs e)
        {
            saveState();
        }

        public void saveState()
        {
            //x
            List<string> badgechangedlist = new List<string>();
            for (int i = 0; i < eaglebadgelist.Count; i++) {
                if (eaglebadgelist[i].recieved == true) {
                    badgechangedlist.Add(eaglebadgelist[i].badgeName);
                }
            }
            using (IsolatedStorageFile appstore = IsolatedStorageFile.GetUserStoreForApplication())
            {
                using (IsolatedStorageFileStream xmlstream = appstore.CreateFile("scout_settings.xml"))
                {
                    foreach (XElement xe in loadeddata.Element("Scout").Element("MeritBadges").Nodes())
                    {
                        //savewriter.WriteStartDocument();
                        if (badgechangedlist.Contains(xe.Value) && (xe.Attribute("rank").Value.Equals("blank") || xe.Attribute("recieved").Value.Equals("star")))
                        {
                            //x
                            if (xe.Attribute("recieved").Value.Equals("false"))
                            {
                                xe.Attribute("recieved").Value = "true";
                                xe.Attribute("rank").Value = "star";
                            }
                            else if (xe.Attribute("recieved").Value.Equals("true"))
                            {
                                xe.Attribute("recieved").Value = "false";
                                xe.Attribute("rank").Value = "blank";
                            }
                        }
                    }

                    foreach (XElement xe in loadeddata.Element("Scout").Element("Ranks").Element("Star").Element("Requirements").Nodes())
                    {
                        //savewriter.WriteStartDocument();
                        if (xe.HasElements == true)
                        {
                            for (int a = 0; a < starpivot.Items.Count; a++)
                            {
                                PivotItem savepivot = (PivotItem)starpivot.Items[a];
                                if (savepivot.Header.Equals(xe.Attribute("id").Value))
                                {
                                    Grid savegrid = (Grid)savepivot.Content;
                                    int b = 0;
                                    foreach (XElement subxe in xe.Nodes())
                                    {
                                        //x
                                        if (subxe.Name.LocalName.Equals("dropdown"))
                                        {
                                            //x
                                            
                                            while (b < savegrid.Children.Count)
                                            {
                                                if (savegrid.Children[b] is ListPicker)
                                                {
                                                    ListPicker savepicker = (ListPicker)savegrid.Children[b];
                                                    string selectedposstring = (string)savepicker.SelectedItem;
                                                    foreach (XElement savedropdownxe in subxe.Nodes())
                                                    {
                                                        //x
                                                        if (savedropdownxe.Value.Equals(selectedposstring)) {
                                                            savedropdownxe.Attribute("selected").Value = "true";
                                                        }
                                                        else if (savedropdownxe.Attribute("selected").Value.Equals("true") && (!savedropdownxe.Value.Equals(selectedposstring))) {
                                                            //x
                                                            savedropdownxe.Attribute("selected").Value = "false";
                                                        }
                                                    }
                                                    b++;
                                                    break;
                                                }
                                                b++;
                                            }
                                        }
                                    }
                                    a = starpivot.Items.Count - 1;
                                }
                            }
                        }
                    }

                    loadeddata.Save(xmlstream);
                }
            }
        }
    }
}