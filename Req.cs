﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Collections.Generic;

namespace BSA_Eagle_Tracker
{
    public class Req
    {
        public bool finished;

        //public string rankname;
        //public string badgenames;
        private string reqid;
        private string description;
        private int[] marginvalues;
        public int descriptionheight;
        private List<SubItem> subitemlist;
        private bool subitemexists;

        public Req()
        {
            //placeholder
        }

        public Req(string tempid, string tempdesc, bool status, string marginstring, string heightstring)
        {
            reqid = tempid;
            description = tempdesc;
            finished = status;
            descriptionheight = int.Parse(heightstring);
            marginvalues = processMargin(marginstring);
            subitemexists = false;
        }

        public Req(string tempid, string tempdesc, bool status, string marginstring, string heightstring, List<SubItem> sublist)
        {
            reqid = tempid;
            description = tempdesc;
            finished = status;
            descriptionheight = int.Parse(heightstring);
            marginvalues = processMargin(marginstring);
            subitemexists = true;
            subitemlist = sublist;
        }

        public string RequirementId
        {
            get { return reqid; }
            set { reqid = value; }
        }

        public string RequirementText
        {
            get {return description;}
            set { description = value; }
        }

        public bool RequirementFinished
        {
            get { return finished; }
            set {finished = value;}
        }

        public List<SubItem> getSubItemList()
        {
            return subitemlist;
        }

        public Thickness getMargin()
        {
            Thickness tempmargin = new Thickness(marginvalues[0], marginvalues[1], marginvalues[2], marginvalues[3]);
            return tempmargin;
        }

        private int[] processMargin(string s)
        {
            //x
            int[] thicknessarray = new int[4];
            string[] sarray = s.Split(' ');
            for (int j = 0; j < sarray.Length; j++)
            {
                //x
                thicknessarray[j] = int.Parse(sarray[j]);
            }
            sarray = null;
            return thicknessarray;
        }

        public bool hasSubItem()
        {
            return subitemexists;
        }
    }
}
