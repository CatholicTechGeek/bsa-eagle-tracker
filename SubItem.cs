﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace BSA_Eagle_Tracker
{
    public class SubItem
    {
        private string subitemtype;
        private string description;
        private int[] marginvalues;
        private string subid;
        public int descriptionheight;
        public int descriptionwidth;
        private bool selectionstate;
        private string subiteminput;

        public string typestring
        {
            get { return subitemtype; }
            private set { subitemtype = value;}
        }

        public string subitemid
        {
            get { return subid; }
            private set { subid = value; }
        }
        public string descstring
        {
            get { return description; }
            private set {description = value;}
        }

        public bool isSelected
        {
            get { return selectionstate; }
            private set { selectionstate = value; }
        }

        public string input
        {
            get { return subiteminput; }
            set { subiteminput = value; }
        }

        public SubItem(string itemtype, string itemid, string itemdescription, string marginstring, string heightstring, bool selected = false)
        {
            subitemtype = itemtype;
            subid = itemid;
            description = itemdescription;
            descriptionheight = int.Parse(heightstring);
            descriptionwidth = 395;
            marginvalues = processMargin(marginstring);
            selectionstate = selected;
        }

        public SubItem(string itemtype, string itemid, string itemdescription, string marginstring, string heightstring, string widthstring, bool selected = false)
        {
            subitemtype = itemtype;
            subid = itemid;
            description = itemdescription;
            descriptionheight = int.Parse(heightstring);
            descriptionwidth = int.Parse(widthstring);
            marginvalues = processMargin(marginstring);
            selectionstate = selected;
        }

        public SubItem(string itemtype, string itemid, string itemdescription, string marginstring, string heightstring, string textvalue)
        {
            subitemtype = itemtype;
            subid = itemid;
            description = itemdescription;
            descriptionheight = int.Parse(heightstring);
            marginvalues = processMargin(marginstring);
            subiteminput = textvalue;
        }

        public SubItem()
        {
            //placeholder
        }

        public Thickness getMargin()
        {
            Thickness tempmargin = new Thickness(marginvalues[0], marginvalues[1], marginvalues[2], marginvalues[3]);
            return tempmargin;
        }

        private int[] processMargin(string s)
        {
            //x
            int[] thicknessarray = new int[4];
            string[] sarray = s.Split(' ');
            for (int j = 0; j < sarray.Length; j++)
            {
                //x
                thicknessarray[j] = int.Parse(sarray[j]);
            }
            return thicknessarray;
        }
    }
}
