﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.IO;
using System.IO.IsolatedStorage;
using System.Windows.Resources;
using System.Xml;
using System.Xml.Linq;

namespace BSA_Eagle_Tracker
{
    public class StorageHelper
    {
        public static void copyToIsolatedStorage(string filetobecopied, string copyfilename)
        {
            //bool successful = false;
            //StreamReader xapreader = new StreamReader(filetobecopied);
            IsolatedStorageFile appstore = IsolatedStorageFile.GetUserStoreForApplication();
            Uri xapcopyuri = new Uri(filetobecopied, UriKind.Relative);
            StreamResourceInfo sri = Application.GetResourceStream(xapcopyuri);
            IsolatedStorageFileStream copystream = appstore.CreateFile(copyfilename);
            BinaryReader bireader = new BinaryReader(sri.Stream);
            int writeLength = Convert.ToInt32(bireader.BaseStream.Length);
            copystream.Write(bireader.ReadBytes(writeLength), 0, writeLength);
            bireader.Close();
            copystream.Close();
            //copystream.Dispose();
        }

        public static XDocument getSettingsXml(string xmlfilename)
        {
            //x
            XDocument loaddoc;
            using (IsolatedStorageFile appstore = IsolatedStorageFile.GetUserStoreForApplication())
            {
                //using (IsolatedStorageFileStream xmlstream = appstore.OpenFile(xmlfilename, FileMode.Open, FileAccess.ReadWrite))
                using (IsolatedStorageFileStream xmlstream = new IsolatedStorageFileStream(xmlfilename,FileMode.Open,FileAccess.Read, appstore))
                {
                    loaddoc = XDocument.Load(xmlstream);
                    xmlstream.Close();
                    //xmlstream.Dispose();
                }
                //appstore.Dispose();
            }
            return loaddoc;
        }

        public static void deleteSettings(string xmlfilename)
        {
            using (IsolatedStorageFile appstore = IsolatedStorageFile.GetUserStoreForApplication())
            {
                if (appstore.FileExists(xmlfilename))
                {
                    appstore.DeleteFile(xmlfilename);
                }
            }
            MessageBox.Show("Press the Back Button to exit and reset");
        }

        public static void saveSettingsXml(string xmlfilename, XDocument settingsdoc)
        {
            //XDocument loaddoc = s;
            using (IsolatedStorageFile appstore = IsolatedStorageFile.GetUserStoreForApplication())
            {
                //using (IsolatedStorageFileStream xmlstream = appstore.OpenFile(xmlfilename, FileMode.Open, FileAccess.ReadWrite))
                using (IsolatedStorageFileStream xmlstream = new IsolatedStorageFileStream(xmlfilename,FileMode.Create, FileAccess.ReadWrite, appstore))
                {
                    settingsdoc.Save(xmlstream);
                    xmlstream.Close();
                    //xmlstream.Dispose();
                }
                //appstore.Dispose();
            }
        }

        public static bool fileInIS(string filenametocheck)
        {
            using (IsolatedStorageFile appstore = IsolatedStorageFile.GetUserStoreForApplication())
            {
                if (appstore.FileExists(filenametocheck))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
    }
}
