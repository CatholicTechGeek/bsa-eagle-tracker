﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Microsoft.Phone.Controls;
using System.Xml.Linq;
using System.IO.IsolatedStorage;
//using System.IO;
using CustomControlLibray;

namespace BSA_Eagle_Tracker
{
    public partial class Life : PhoneApplicationPage
    {
        private List<Req> requirementlist;
        private static XDocument loadeddata;

        public Life()
        {
            InitializeComponent();
            loadeddata = StorageHelper.getSettingsXml("scout_settings.xml");
            requirementlist = getRequirementList();
            setRequirements();
        }

        protected override void OnNavigatedTo(System.Windows.Navigation.NavigationEventArgs e)
        {
            if (e.NavigationMode.Equals(System.Windows.Navigation.NavigationMode.Back))
            {
                if (loadeddata != null)
                {
                    loadeddata = null;
                }
                loadeddata = StorageHelper.getSettingsXml("scout_settings.xml");
            }
            base.OnNavigatedTo(e);
        }

        public static List<Req> getRequirementList()
        {
            //x
            List<Req> reqlist = new List<Req>();
            //XDocument loadeddata = StorageHelper.getSettingsXml("scout_settings.xml");
            /*
            var data = from query in loadeddata.Element("Tenderfoot").Descendants()
                       select new Req
                       {
                           reqid = (string)query.Attribute("id"),
                           RequirementText = (string)query.Element("req"),
                           RequirementFinished = bool.Parse(query.Attribute("used").Value)
                           
                       };
            */
            if (loadeddata == null)
            {
                loadeddata = StorageHelper.getSettingsXml("scout_settings.xml");
            }
            var data = from query in loadeddata.Element("Scout").Element("Ranks").Element("Life").Element("Requirements").Nodes()
                       select query;

            foreach (XElement xe in data)
            {
                Req tempreq;
                bool tempstatus = bool.Parse(xe.Attribute("done").Value);
                /*
                if (xe.HasElements == true && xe.Name.LocalName.Equals("dropdown"))
                {
                    //x
                    List<SubItem> elementsubitems = new List<SubItem>();
                    foreach (XElement subxe in xe.Element("dropdown").Nodes())
                    {
                        SubItem tempsubitem = new SubItem(subxe.Name.LocalName, subxe.Value, subxe.Attribute("margin").Value, subxe.Attribute("height").Value);
                        elementsubitems.Add(tempsubitem);
                    }
                }
                 */
                if (xe.HasElements == true)
                {
                    //x
                    List<SubItem> elementsubitems = new List<SubItem>();
                    foreach (XElement subxe in xe.Nodes())
                    {
                        //x
                        if (subxe.Name.LocalName.Equals("dropdown"))
                        {
                            //x
                            foreach (XElement dropdownxe in subxe.Nodes())
                            {
                                //x
                                bool dropdownchosen = bool.Parse(dropdownxe.Attribute("selected").Value);
                                if (dropdownchosen == true)
                                {
                                    SubItem tempsubitem = new SubItem(dropdownxe.Name.LocalName, xe.Attribute("id").Value, dropdownxe.Value, dropdownxe.Attribute("margin").Value, dropdownxe.Attribute("height").Value, dropdownchosen);
                                    elementsubitems.Add(tempsubitem);
                                }
                                else
                                {
                                    SubItem tempsubitem = new SubItem(dropdownxe.Name.LocalName, xe.Attribute("id").Value, dropdownxe.Value, dropdownxe.Attribute("margin").Value, dropdownxe.Attribute("height").Value);
                                    elementsubitems.Add(tempsubitem);
                                }
                            }
                        }
                        else
                        {
                            SubItem tempsubitem = new SubItem(subxe.Name.LocalName, xe.Attribute("id").Value, subxe.Value, subxe.Attribute("margin").Value, subxe.Attribute("height").Value);
                            elementsubitems.Add(tempsubitem);
                        }
                    }
                    tempreq = new Req(xe.Attribute("id").Value, xe.Attribute("desc").Value, tempstatus, xe.Attribute("margin").Value, xe.Attribute("height").Value, elementsubitems);
                    reqlist.Add(tempreq);
                }
                else
                {
                    tempreq = new Req(xe.Attribute("id").Value, xe.Attribute("desc").Value, tempstatus, xe.Attribute("margin").Value, xe.Attribute("height").Value);
                    reqlist.Add(tempreq);
                }
            }
            return reqlist;
        }

        private void setRequirements()
        {
            //x
            int dropdownnum = ContentPanel.Children.Count - 1;

            //the first multi-dropdown panel
            reqdropdown1.Checkboxid = "Requirement " + requirementlist[0].RequirementId;
            reqdropdown1.descriptionstring = requirementlist[0].RequirementText;
            if (requirementlist[0].finished == true)
            {
                reqdropdown1.Checkedoff = true;
            }

            if (requirementlist[0].hasSubItem())
            {
                //x
                List<SubItem> subitemvalues = requirementlist[0].getSubItemList();
                SolidColorBrush subitembrush = new SolidColorBrush(Colors.White);
                //ScrollViewer subitemviewer = new ScrollViewer();
                //StackPanel subitempanel = new StackPanel();
                for (int j = 0; j < subitemvalues.Count; j++)
                {
                    //x
                    if (subitemvalues[j].typestring.Equals("check"))
                    {
                        CheckBox tempcheck = new CheckBox();
                        tempcheck.Content = subitemvalues[j].descstring;
                        tempcheck.Name = subitemvalues[j].subitemid;
                        tempcheck.Height = subitemvalues[j].descriptionheight;
                        tempcheck.HorizontalAlignment = HorizontalAlignment.Left;
                        //firstblock.Margin = new Thickness(14,163,0,0);
                        //tempcheck.Margin = subitemvalues[j].getMargin();
                        tempcheck.VerticalAlignment = VerticalAlignment.Top;
                        tempcheck.Foreground = subitembrush;
                        tempcheck.Background = subitembrush;
                        //tempcheck.Width = 300;
                        if (subitemvalues[j].isSelected == true)
                        {
                            tempcheck.IsChecked = true;
                        }
                        reqdropdown1.addToDropdown(tempcheck);
                        //subitempanel.Children.Add(tempcheck);
                    }
                    else if (subitemvalues[j].typestring.Equals("dropdownitem"))
                    {
                        //this.DataContext = this;
                        //List<string> dropdownlistitems = new List<string>();
                        List<string> subitemchoices = new List<string>();
                        ListPicker choicelist = new ListPicker();
                        //dropdownlistitems.Add(subitemvalues[j]);
                        int dropdowncounter = j;
                        //dropdowncounter++;
                        while ((dropdowncounter < (subitemvalues.Count)) && (subitemvalues[dropdowncounter].typestring.Equals("dropdownitem")))
                        {
                            //ListPickerItem tempitem = new ListPickerItem();
                            //tempitem.Content = subitemvalues[dropdowncounter].descstring;
                            //tempitem.Height = subitemvalues[dropdowncounter].descriptionheight;
                            //tempitem.Width = 300;
                            subitemchoices.Add(subitemvalues[dropdowncounter].descstring);
                            dropdowncounter++;
                        }
                        choicelist.ItemsSource = subitemchoices;
                        //choicelist.Margin = subitemvalues[j].getMargin();
                        //firstgrid.Children.Add(choicelist);
                        reqdropdown1.addToDropdown(choicelist);
                        j = dropdowncounter;
                    }
                }
            }

            //the rest of the multi-dropdown panels
            //loop for remaining items
            for (int i = 1; i < requirementlist.Count; i++)
            {
                if (requirementlist[i].RequirementId.Equals("3"))
                {
                    SolidColorBrush subitembrush = new SolidColorBrush(Colors.White);
                    ButtonDropdown tempbuttondrop = new ButtonDropdown();
                    tempbuttondrop.HorizontalAlignment = HorizontalAlignment.Left;
                    tempbuttondrop.VerticalAlignment = VerticalAlignment.Top;
                    tempbuttondrop.Checkboxid = "Requirement " + requirementlist[i].RequirementId;
                    tempbuttondrop.descriptionstring = requirementlist[i].RequirementText;
                    if (requirementlist[i].finished == true)
                    {
                        tempbuttondrop.Checkedoff = true;
                    }
                    Button mbbutton = new Button();
                    mbbutton.Content = "Goto Merit Badge List";
                    mbbutton.Click += new RoutedEventHandler(mbbutton_Click);
                    mbbutton.Foreground = subitembrush;
                    tempbuttondrop.addToDropdown(mbbutton);
                    reqpanel.Children.Add(tempbuttondrop);
                    dropdownnum++;
                }
                else
                {
                    ButtonDropdown tempbuttondrop = new ButtonDropdown();
                    tempbuttondrop.HorizontalAlignment = HorizontalAlignment.Left;
                    tempbuttondrop.VerticalAlignment = VerticalAlignment.Top;
                    tempbuttondrop.Checkboxid = "Requirement " + requirementlist[i].RequirementId;
                    tempbuttondrop.descriptionstring = requirementlist[i].RequirementText;
                    if (requirementlist[i].finished == true)
                    {
                        tempbuttondrop.Checkedoff = true;
                    }
                    /*
                    tempblock.Height = requirementlist[i].descriptionheight;
                    tempblock.HorizontalAlignment = HorizontalAlignment.Left;
                    //tempblock.Margin = new Thickness(14, 163, 0, 0);
                    tempblock.Margin = requirementlist[i].getMargin();
                    tempblock.VerticalAlignment = VerticalAlignment.Top;
                    tempblock.Width = 450;
                    tempblock.TextWrapping = TextWrapping.Wrap;
                    Grid tempgrid = new Grid();
                    tempgrid.Children.Add(tempblock);
                     */
                    if (requirementlist[i].hasSubItem())
                    {
                        //x
                        List<SubItem> subitemvalues = requirementlist[i].getSubItemList();
                        SolidColorBrush subitembrush = new SolidColorBrush(Colors.White);
                        for (int j = 0; j < subitemvalues.Count; j++)
                        {
                            //x
                            if (subitemvalues[j].typestring.Equals("check"))
                            {
                                CheckBox tempcheck = new CheckBox();
                                tempcheck.Content = subitemvalues[j].descstring;
                                tempcheck.Name = subitemvalues[j].subitemid;
                                tempcheck.Height = subitemvalues[j].descriptionheight;
                                tempcheck.HorizontalAlignment = HorizontalAlignment.Left;
                                tempcheck.Foreground = subitembrush;
                                tempcheck.Background = subitembrush;
                                //firstblock.Margin = new Thickness(14,163,0,0);
                                // tempcheck.Margin = subitemvalues[j].getMargin();
                                tempcheck.VerticalAlignment = VerticalAlignment.Top;
                                tempcheck.Width = subitemvalues[j].descriptionwidth;
                                if (subitemvalues[j].isSelected == true)
                                {
                                    tempcheck.IsChecked = true;
                                }
                                tempbuttondrop.addToDropdown(tempcheck);
                            }
                            else if (subitemvalues[j].typestring.Equals("label"))
                            {
                                //x
                                TextBlock continueblock = new TextBlock();
                                continueblock.Text = subitemvalues[j].descstring;
                                continueblock.Name = subitemvalues[j].subitemid;
                                continueblock.Height = subitemvalues[j].descriptionheight;
                                continueblock.HorizontalAlignment = HorizontalAlignment.Left;
                                continueblock.Foreground = subitembrush;
                                //firstblock.Margin = new Thickness(14,163,0,0);
                                //continueblock.Margin = subitemvalues[j].getMargin();
                                continueblock.VerticalAlignment = VerticalAlignment.Top;
                                continueblock.Width = 300;
                                tempbuttondrop.addToDropdown(continueblock);

                            }
                            else if (subitemvalues[j].typestring.Equals("textentry"))
                            {
                                //x
                                TextBox entrybox = new TextBox();
                                TextBlock entryheader = new TextBlock();
                                entrybox.Name = subitemvalues[j].subitemid;
                                entryheader.Text = subitemvalues[j].descstring;
                                entryheader.Height = subitemvalues[j].descriptionheight;
                                entrybox.Height = subitemvalues[j].descriptionheight;
                                entryheader.Width = 300;
                                entrybox.Width = 300;
                                entryheader.Foreground = subitembrush;
                                //entryheader.Margin = subitemvalues[j].getMargin();
                                //entrybox.Margin = new Thickness(entryheader.Margin.Left, (entryheader.Margin.Top + 20), entryheader.Margin.Right, entryheader.Margin.Bottom);
                                if (!subitemvalues[j].input.Equals(" "))
                                {
                                    entrybox.Text = subitemvalues[j].input;
                                }
                                entryheader.HorizontalAlignment = HorizontalAlignment.Left;
                                entrybox.HorizontalAlignment = HorizontalAlignment.Left;
                                entryheader.VerticalAlignment = VerticalAlignment.Top;
                                entrybox.VerticalAlignment = VerticalAlignment.Top;
                                StackPanel entrypanel = new StackPanel();
                                entrypanel.Orientation = System.Windows.Controls.Orientation.Horizontal;
                                entrypanel.Children.Add(entryheader);
                                entrypanel.Children.Add(entrybox);
                                tempbuttondrop.addToDropdown(entrypanel);
                            }
                            else if (subitemvalues[j].typestring.Equals("dropdownitem"))
                            {
                                //this.DataContext = this;
                                //List<string> dropdownlistitems = new List<string>();
                                List<string> subitemchoices = new List<string>();
                                ListPicker choicelist = new ListPicker();
                                //dropdownlistitems.Add(subitemvalues[j]);
                                int dropdowncounter = j;
                                //dropdowncounter++;
                                while ((dropdowncounter < (subitemvalues.Count)) && (subitemvalues[dropdowncounter].typestring.Equals("dropdownitem")))
                                {
                                    //ListPickerItem tempitem = new ListPickerItem();
                                    //tempitem.Content = subitemvalues[dropdowncounter].descstring;
                                    //tempitem.Height = subitemvalues[dropdowncounter].descriptionheight;
                                    //tempitem.Width = 300;
                                    subitemchoices.Add(subitemvalues[dropdowncounter].descstring);
                                    dropdowncounter++;
                                }
                                choicelist.ItemsSource = subitemchoices;
                                //choicelist.Margin = subitemvalues[j].getMargin();
                                //firstgrid.Children.Add(choicelist);
                                tempbuttondrop.addToDropdown(choicelist);
                                j = dropdowncounter;
                            }
                        }
                    }
                    //add new dropdownbox to panel
                    //int childnum = ContentPanel.Children.Count;
                    //ButtonDropdown margindropdown = ContentPanel.Children.Cast<ButtonDropdown>().ElementAt(dropdownnum);
                    //tempbuttondrop.Margin = new Thickness(margindropdown.Margin.Left, (margindropdown.Margin.Top + margindropdown.Height + 20), margindropdown.Margin.Right, margindropdown.Margin.Bottom);
                    reqpanel.Children.Add(tempbuttondrop);
                    dropdownnum++;
                }
                //the end
            }
            //the end
        }

        private void mbbutton_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new Uri("/MeritBadges.xaml?", UriKind.Relative));
        }

        protected override void OnNavigatedFrom(System.Windows.Navigation.NavigationEventArgs e)
        {
            saveState();
        }

        public void saveState()
        {
            int reqnum = int.Parse(loadeddata.Element("Scout").Element("Ranks").Element("Life").Element("Requirements").Attribute("num").Value);
            int completecount = 0;

            using (IsolatedStorageFile appstore = IsolatedStorageFile.GetUserStoreForApplication())
            {
                using (IsolatedStorageFileStream xmlstream = new IsolatedStorageFileStream("scout_settings.xml", System.IO.FileMode.Create, System.IO.FileAccess.ReadWrite, appstore))
                {
                    //x
                    int n = 0;
                    ButtonDropdown[] saveitems = reqpanel.Children.Cast<ButtonDropdown>().ToArray();
                    foreach (XElement xe in loadeddata.Element("Scout").Element("Ranks").Element("Life").Element("Requirements").Nodes())
                    {
                        //x
                        //string realid = saveitems[n].Checkboxid.Substring(12,(saveitems[n].Checkboxid.Length - 12));
                        if (saveitems[n].Checkedoff == true)
                        {
                            xe.Attribute("done").Value = "true";
                            completecount++;
                        }
                        else
                        {
                            xe.Attribute("done").Value = "false";
                        }

                        //handling for any subitems
                        if (xe.HasElements)
                        {
                            StackPanel savepanel = saveitems[n].getItemsPanel();

                            //make subcount start at 1 since 1st item is always textblock
                            int subcount = 1;
                            foreach (XElement subxe in xe.Nodes())
                            {
                                //x
                                if (subxe.Name.LocalName.Equals("dropdown"))
                                {
                                    //x
                                    ListPicker savepicker = (ListPicker)savepanel.Children[subcount];
                                    string selectedposstring = (string)savepicker.SelectedItem;
                                    foreach (XElement savedropdownxe in subxe.Nodes())
                                    {
                                        if (savedropdownxe.Value.Equals(selectedposstring))
                                        {
                                            //x
                                            savedropdownxe.Attribute("selected").Value = "true";
                                        }
                                        else
                                        {
                                            savedropdownxe.Attribute("selected").Value = "false";
                                        }
                                    }
                                }
                                if ((subcount + 1) < savepanel.Children.Count)
                                {
                                    subcount++;
                                }
                            }
                        }

                        if ((n + 1) < saveitems.Length)
                        {
                            n++;
                        }
                    }
                    if (completecount == reqnum && loadeddata.Element("Scout").Element("information").Element("currentrank").Value.Equals("Star"))
                    {
                        loadeddata.Element("Scout").Element("information").Element("previousrank").Value = loadeddata.Element("Scout").Element("information").Element("currentrank").Value;
                        loadeddata.Element("Scout").Element("information").Element("currentrank").Value = "Life";
                    }
                    else if (loadeddata.Element("Scout").Element("information").Element("currentrank").Value.Equals("Life"))
                    {
                        loadeddata.Element("Scout").Element("information").Element("currentrank").Value = loadeddata.Element("Scout").Element("information").Element("previousrank").Value;
                    }
                    loadeddata.Save(xmlstream);
                    xmlstream.Close();
                }
            }
        }
    }
}