﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Microsoft.Phone.Controls;
using System.Xml.Linq;
using System.IO.IsolatedStorage;
using System.IO;

namespace BSA_Eagle_Tracker
{
    public partial class MeritBadges : PhoneApplicationPage
    {
        private XDocument loadeddata;
        private List<string> completedeaglebadges;
        private List<string> completedoptionalbadges;
        private List<CheckBox> optionalnames;
        private List<Group<CheckBox>> groupselection;
        //private ListPicker[] optionalchoices;
        //private LongListSelector optionalchoices;
        private CheckBox[] eaglechecks;
        List<string> optionaleaglebadges;

        public MeritBadges()
        {
            InitializeComponent();
            this.DataContext = this;
            completedeaglebadges = new List<string>();
            completedoptionalbadges = new List<string>();
            eaglechecks = new CheckBox[15];
            optionaleaglebadges = new List<string>();
            //optionalchoices = new ListPicker[9];
            //optionalchoices = new LongListSelector();
            loadeddata = StorageHelper.getSettingsXml("scout_settings.xml");
            setComponents();
        }

        public void setComponents()
        {
            //get the Eagle-reqired Badges first
            MeritBadge[] eaglelist = getMeritBadgeArray(true);
            int m = 0;
            for (int i = 0; i < eaglelist.Length; i++)
            {
                //x
                if (i < 15)
                {
                    m = i;
                }
                if (eaglelist[i].eagleRequired == true)
                {
                    //x
                    eaglechecks[m] = new CheckBox();
                    eaglechecks[m].Content = eaglelist[i].badgeName;
                    eaglechecks[m].Checked += new RoutedEventHandler(eaglecheck_Checked);
                    eaglechecks[m].Unchecked += new RoutedEventHandler(eaglecheck_Unchecked);
                    if (eaglelist[i].recieved == true)
                    {
                        eaglechecks[m].IsChecked = true;
                    }
                    mbrequiredpanel.Children.Add(eaglechecks[m]);
                }
            }

            //now for the optional merit badges, has to be List due to optional Eagle badges
            //MeritBadge[] optionallist = getMeritBadgeArray(false);
            optionalnames = getOptionalNames();
            //optionalchoices.IsFlatList = true;
            //optionalchoices.ItemsSource = optionalnames;
            //optgrid.Children.Add(optionalchoices);
            //optlist.ItemsSource = optionalnames;
            groupselection = (from selection in optionalnames group selection by (selection.Content as string).Substring(0, 1) into c select new Group<CheckBox>(c.Key, c)).ToList();
            if (!groupselection[0].Key.Equals("A"))
            {
                groupselection.Sort(new MeritBadges.GroupComp());
            }
            optlist.ItemsSource = groupselection;
            //groupselection = null;
            //optlist.SelectionMode = SelectionMode.Multiple;
        }

        private List<CheckBox> getOptionalNames()
        {
            List<CheckBox> optnames = new List<CheckBox>();
            MeritBadge[] tempopt = getMeritBadgeArray(false);
            for (int ij = 0; ij < tempopt.Length; ij++)
            {
                //x
                CheckBox listtext = new CheckBox();
                listtext.Content = tempopt[ij].badgeName;
                listtext.FontSize = 26;
                listtext.Checked += new RoutedEventHandler(optioncheck_Checked);
                listtext.Unchecked += new RoutedEventHandler(optioncheck_Unchecked);
                if (tempopt[ij].recieved == true)
                {
                    listtext.IsChecked = true;
                }
                optnames.Add(listtext);
            }
            return optnames;
        }

        private MeritBadge[] getMeritBadgeArray(bool eaglerequired)
        {
            List<MeritBadge> eblist = new List<MeritBadge>();
            var mbdata = from query in loadeddata.Element("Scout").Element("MeritBadges").Nodes()
                         select query;
            

            //all eagle-required badges
            if (eaglerequired)
            {
                foreach (XElement xe in mbdata)
                {
                    bool iseaglerequired = bool.Parse(xe.Attribute("eaglerequired").Value);

                    if (iseaglerequired)
                    {
                        if (!optionaleaglebadges.Contains(xe.Value))
                        {
                            MeritBadge tempbadge = new MeritBadge(xe.Value, true, bool.Parse(xe.Attribute("recieved").Value));
                            if (bool.Parse(xe.Attribute("recieved").Value) == true)
                            {
                                completedeaglebadges.Add(xe.Value);
                                //handling the eagle-required badges with options
                                if (xe.Value.Contains("Emergency Preparedness"))
                                {
                                    optionaleaglebadges.Add("Lifesaving");
                                }
                                else if (xe.Value.Contains("Lifesaving"))
                                {
                                    optionaleaglebadges.Add("Emergency Preparedness");
                                }
                                else if (xe.Value.Contains("Swimming"))
                                {
                                    optionaleaglebadges.Add("Hiking");
                                    optionaleaglebadges.Add("Cycling");
                                }
                                else if (xe.Value.Contains("Hiking"))
                                {
                                    optionaleaglebadges.Add("Swimming");
                                    optionaleaglebadges.Add("Cycling");
                                }
                                else if (xe.Value.Contains("Cycling"))
                                {
                                    optionaleaglebadges.Add("Swimming");
                                    optionaleaglebadges.Add("Hiking");
                                }
                            }
                            eblist.Add(tempbadge);
                        }
                        //the following is to handle eagle-required with options
                        else if (bool.Parse(xe.Attribute("recieved").Value) == true)
                        {
                            completedoptionalbadges.Add(xe.Value);
                        }
                    }
                }
            }
                //all optional badges
            else
            {
                for (int i = 0; i < optionaleaglebadges.Count; i++)
                {
                    if (completedoptionalbadges.Contains(optionaleaglebadges[i]))
                    {
                        MeritBadge tempbadge = new MeritBadge(optionaleaglebadges[i], true, true);
                        eblist.Add(tempbadge);
                    }
                    else
                    {
                        MeritBadge tempbadge = new MeritBadge(optionaleaglebadges[i], true, false);
                        eblist.Add(tempbadge);
                    }
                }
                foreach (XElement xe in mbdata)
                {
                    bool iseaglerequired = bool.Parse(xe.Attribute("eaglerequired").Value);

                    if (!iseaglerequired)
                    {
                        MeritBadge tempbadge = new MeritBadge(xe.Value, false, bool.Parse(xe.Attribute("recieved").Value));
                        if (bool.Parse(xe.Attribute("recieved").Value) == true)
                        {
                            completedoptionalbadges.Add(xe.Value);
                        }
                        eblist.Add(tempbadge);
                    }
                }
            }
            return eblist.ToArray();
        }

        private List<MeritBadge> getMeritBadgeList(bool eaglerequired)
        {
            List<MeritBadge> eblist = new List<MeritBadge>();
            var mbdata = from query in loadeddata.Element("Scout").Element("MeritBadges").Nodes()
                         select query;

            //all eagle-required badges
            if (eaglerequired)
            {
                foreach (XElement xe in mbdata)
                {
                    bool iseaglerequired = bool.Parse(xe.Attribute("eaglerequired").Value);

                    if (iseaglerequired)
                    {
                        MeritBadge tempbadge = new MeritBadge(xe.Value, true, bool.Parse(xe.Attribute("recieved").Value));
                        eblist.Add(tempbadge);
                    }
                }
            }
            //all optional badges
            else
            {
                foreach (XElement xe in mbdata)
                {
                    bool iseaglerequired = bool.Parse(xe.Attribute("eaglerequired").Value);

                    if (!iseaglerequired)
                    {
                        MeritBadge tempbadge = new MeritBadge(xe.Value, false, bool.Parse(xe.Attribute("recieved").Value));
                        eblist.Add(tempbadge);
                    }
                }
            }
            return eblist;
        }

        private void optioncheck_Checked(object sender, RoutedEventArgs e)
        {
            //x
            CheckBox checkedoff = sender as CheckBox;
            string completedname = (string)checkedoff.Content;
            completedoptionalbadges.Add(completedname);
        }

        private void optioncheck_Unchecked(object sender, RoutedEventArgs e)
        {
            //x
            CheckBox checkedoff = sender as CheckBox;
            string completedname = (string)checkedoff.Content;
            completedoptionalbadges.Remove(completedname);
        }


        private void eaglecheck_Checked(object sender, RoutedEventArgs e)
        {
            //completed[headerpicker.SelectedIndex] = true;
            CheckBox checkedoff = sender as CheckBox;
            string completedname = (string)checkedoff.Content;
            completedeaglebadges.Add(completedname);

            //disable the optional ones if option merit badge is checked
            if (completedname.Equals("Emergency Preparedness") || completedname.Equals("Lifesaving"))
            {
                //x
                if (completedname.Equals("Emergency Preparedness"))
                {
                    //x
                    for (int i = 0; i < eaglechecks.Length; i++)
                    {
                        try
                        {
                            string mbnamestring = (string)eaglechecks[i].Content;
                            if (mbnamestring.Equals("Lifesaving"))
                            {
                                eaglechecks[i].Visibility = System.Windows.Visibility.Collapsed;
                                CheckBox reqopt = new CheckBox();
                                reqopt.Content = mbnamestring;
                                reqopt.FontSize = 26;
                                reqopt.Checked += new RoutedEventHandler(optioncheck_Checked);
                                reqopt.Unchecked += new RoutedEventHandler(optioncheck_Unchecked);
                                int addpos = groupselection.Select((item, j) => new { Item = item, Index = j })
                    .Single(x => x.Item.Key == "L").Index;
                                groupselection[addpos].Add(reqopt);
                                optlist.ItemsSource = null;
                                optlist.ItemsSource = groupselection;
                                break;
                                //listitems.Add(reqopt);
                                //TextComp tex = new TextComp();

                            }
                        }
                        catch (NullReferenceException)
                        {
                            break;
                        }
                    }
                }
                else if (completedname.Equals("Lifesaving"))
                {
                    //x
                    for (int i = 0; i < eaglechecks.Length; i++)
                    {
                        try
                        {
                            string mbnamestring = (string)eaglechecks[i].Content;
                            if (mbnamestring.Equals("Emergency Preparedness"))
                            {
                                eaglechecks[i].Visibility = System.Windows.Visibility.Collapsed;
                                CheckBox reqopt = new CheckBox();
                                reqopt.Content = mbnamestring;
                                reqopt.FontSize = 26;
                                reqopt.Checked += new RoutedEventHandler(optioncheck_Checked);
                                reqopt.Unchecked += new RoutedEventHandler(optioncheck_Unchecked);
                                int addpos = groupselection.Select((item, j) => new { Item = item, Index = j })
                    .Single(x => x.Item.Key == "E").Index;
                                groupselection[addpos].Add(reqopt);
                                optlist.ItemsSource = null;
                                optlist.ItemsSource = groupselection;
                                break;
                            }
                        }
                        catch (NullReferenceException)
                        {
                            continue;
                        }
                        catch (ArgumentException)
                        {
                            continue;
                        }
                    }
                }
            }
            else if (completedname.Equals("Swimming") || completedname.Equals("Hiking") || completedname.Equals("Cycling"))
            {
                optlist.ItemsSource = null;
                if (completedname.Equals("Swimming"))
                {
                    //x
                    
                    for (int i = 0; i < eaglechecks.Length; i++)
                    {
                        try
                        {
                            string mbnamestring = (string)eaglechecks[i].Content;
                            if (mbnamestring.Equals("Hiking"))
                            {
                                eaglechecks[i].Visibility = System.Windows.Visibility.Collapsed;
                                CheckBox reqopt = new CheckBox();
                                reqopt.Content = mbnamestring;
                                reqopt.FontSize = 26;
                                reqopt.Checked += new RoutedEventHandler(optioncheck_Checked);
                                reqopt.Unchecked += new RoutedEventHandler(optioncheck_Unchecked);
                                int addpos = groupselection.Select((item, j) => new { Item = item, Index = j })
                    .Single(x => x.Item.Key == "H").Index;
                                groupselection[addpos].Add(reqopt);
                                //optlist.ItemsSource = groupselection.AsEnumerable();
                            }
                            else if (mbnamestring.Equals("Cycling"))
                            {
                                eaglechecks[i].Visibility = System.Windows.Visibility.Collapsed;
                                CheckBox reqopt = new CheckBox();
                                reqopt.Content = mbnamestring;
                                reqopt.FontSize = 26;
                                reqopt.Checked += new RoutedEventHandler(optioncheck_Checked);
                                reqopt.Unchecked += new RoutedEventHandler(optioncheck_Unchecked);
                                int addpos = groupselection.Select((item, j) => new { Item = item, Index = j })
                    .Single(x => x.Item.Key == "C").Index;
                                groupselection[addpos].Add(reqopt);
                                break;
                                //optlist.ItemsSource = groupselection.AsEnumerable();
                            }
                        }
                        catch (NullReferenceException)
                        {
                            continue;
                        }
                    }
                    //optlist.ItemsSource = null;
                    optlist.ItemsSource = groupselection;
                }
                else if (completedname.Equals("Hiking"))
                {
                    //x
                    //List<Group<CheckBox>> groupselection = optlist.ItemsSource.Cast<Group<CheckBox>>().ToList();
                    for (int i = 0; i < eaglechecks.Length; i++)
                    {
                        try
                        {
                            string mbnamestring = (string)eaglechecks[i].Content;
                            if (mbnamestring.Equals("Swimming"))
                            {
                                eaglechecks[i].Visibility = System.Windows.Visibility.Collapsed;
                                CheckBox reqopt = new CheckBox();
                                reqopt.Content = mbnamestring;
                                reqopt.FontSize = 26;
                                reqopt.Checked += new RoutedEventHandler(optioncheck_Checked);
                                reqopt.Unchecked += new RoutedEventHandler(optioncheck_Unchecked);
                                int addpos = groupselection.Select((item, j) => new { Item = item, Index = j })
                    .Single(x => x.Item.Key == "S").Index;
                                groupselection[addpos].Add(reqopt);
                                //optlist.ItemsSource = groupselection.AsEnumerable();
                            }
                            else if (mbnamestring.Equals("Cycling"))
                            {
                                eaglechecks[i].Visibility = System.Windows.Visibility.Collapsed;
                                CheckBox reqopt = new CheckBox();
                                reqopt.Content = mbnamestring;
                                reqopt.FontSize = 26;
                                reqopt.Checked += new RoutedEventHandler(optioncheck_Checked);
                                reqopt.Unchecked += new RoutedEventHandler(optioncheck_Unchecked);
                                int addpos = groupselection.Select((item, j) => new { Item = item, Index = j })
                    .Single(x => x.Item.Key == "C").Index;
                                groupselection[addpos].Add(reqopt);
                                break;
                                //optlist.ItemsSource = groupselection.AsEnumerable();
                            }
                        }
                        catch (NullReferenceException)
                        {
                            continue;
                        }
                        catch (ArgumentException)
                        {
                            continue;
                        }
                    }
                    //optlist.ItemsSource = null;
                    optlist.ItemsSource = groupselection;
                }
                else if (completedname.Equals("Cycling"))
                {
                    //x
                    //List<Group<CheckBox>> groupselection = optlist.ItemsSource.Cast<Group<CheckBox>>().ToList();
                    for (int i = 0; i < eaglechecks.Length; i++)
                    {
                        try
                        {
                            string mbnamestring = (string)eaglechecks[i].Content;
                            if (mbnamestring.Equals("Swimming"))
                            {
                                eaglechecks[i].Visibility = System.Windows.Visibility.Collapsed;
                                CheckBox reqopt = new CheckBox();
                                reqopt.Content = mbnamestring;
                                reqopt.FontSize = 26;
                                reqopt.Checked += new RoutedEventHandler(optioncheck_Checked);
                                reqopt.Unchecked += new RoutedEventHandler(optioncheck_Unchecked);
                                int addpos = groupselection.Select((item, j) => new { Item = item, Index = j })
                    .Single(x => x.Item.Key == "S").Index;
                                groupselection[addpos].Add(reqopt);
                                //optlist.ItemsSource = groupselection.AsEnumerable();
                            }
                            else if (mbnamestring.Equals("Hiking"))
                            {
                                eaglechecks[i].Visibility = System.Windows.Visibility.Collapsed;
                                CheckBox reqopt = new CheckBox();
                                reqopt.Content = mbnamestring;
                                reqopt.FontSize = 26;
                                reqopt.Checked += new RoutedEventHandler(optioncheck_Checked);
                                reqopt.Unchecked += new RoutedEventHandler(optioncheck_Unchecked);
                                int addpos = groupselection.Select((item, j) => new { Item = item, Index = j })
                    .Single(x => x.Item.Key == "H").Index;
                                groupselection[addpos].Add(reqopt);
                                break;
                                //optlist.ItemsSource = groupselection.AsEnumerable();
                            }
                        }
                        catch (NullReferenceException)
                        {
                            continue;
                        }
                        catch (ArgumentException)
                        {
                            continue;
                        }
                    }
                    //optlist.ItemsSource = null;
                    optlist.ItemsSource = groupselection;
                }
            }
        }

        private void eaglecheck_Unchecked(object sender, RoutedEventArgs e)
        {
            CheckBox checkedoff = sender as CheckBox;
            string completedname = (string)checkedoff.Content;
            completedeaglebadges.Remove(completedname);

            //reenable the optional ones if option merit badge is checked
            if (completedname.Equals("Emergency Preparedness") || completedname.Equals("Lifesaving"))
            {
                //x
                if (completedname.Equals("Emergency Preparedness"))
                {
                    //x
                    for (int i = 0; i < eaglechecks.Length; i++)
                    {
                        try
                        {
                            string mbnamestring = (string)eaglechecks[i].Content;
                            if (mbnamestring.Equals("Lifesaving"))
                            {
                                eaglechecks[i].Visibility = System.Windows.Visibility.Visible;

                            }
                        }
                        catch (NullReferenceException)
                        {
                            eaglechecks[i] = new CheckBox();
                            eaglechecks[i].Content = "Lifesaving";
                            eaglechecks[i].Checked += new RoutedEventHandler(eaglecheck_Checked);
                            eaglechecks[i].Unchecked += new RoutedEventHandler(eaglecheck_Unchecked);
                            mbrequiredpanel.Children.Insert(7, eaglechecks[i]);
                            break;
                        }
                    }
                    removeOptionalBadge("Lifesaving");
                }
                else if (completedname.Equals("Lifesaving"))
                {
                    //x
                    for (int i = 0; i < eaglechecks.Length; i++)
                    {
                        string mbnamestring = (string)eaglechecks[i].Content;
                        if (mbnamestring.Equals("Emergency Preparedness"))
                        {
                            eaglechecks[i].Visibility = System.Windows.Visibility.Visible;
                            //removeOptionalBadge("Emergency Preparedness");
                        }
                    }
                    removeOptionalBadge("Emergency Preparedness");
                }
            }
            else if (completedname.Equals("Swimming") || completedname.Equals("Hiking") || completedname.Equals("Cycling"))
            {
                //x
                if (completedname.Equals("Swimming"))
                {
                    //x
                    for (int i = 0; i < eaglechecks.Length; i++)
                    {
                        try
                        {
                            string mbnamestring = (string)eaglechecks[i].Content;
                            if (mbnamestring.Equals("Hiking"))
                            {
                                eaglechecks[i].Visibility = System.Windows.Visibility.Visible;
                            }
                            else if (mbnamestring.Equals("Cycling"))
                            {
                                eaglechecks[i].Visibility = System.Windows.Visibility.Visible;
                            }
                        }
                        catch (NullReferenceException)
                        {
                            eaglechecks[i] = new CheckBox();
                            eaglechecks[i].Content = "Hiking";
                            eaglechecks[i].Checked += new RoutedEventHandler(eaglecheck_Checked);
                            eaglechecks[i].Unchecked += new RoutedEventHandler(eaglecheck_Unchecked);
                            mbrequiredpanel.Children.Insert(11, eaglechecks[i]);
                            eaglechecks[i + 1] = new CheckBox();
                            eaglechecks[i + 1].Content = "Cycling";
                            eaglechecks[i + 1].Checked += new RoutedEventHandler(eaglecheck_Checked);
                            eaglechecks[i + 1].Unchecked += new RoutedEventHandler(eaglecheck_Unchecked);
                            mbrequiredpanel.Children.Insert(12, eaglechecks[i + 1]);
                            break;
                        }
                    }
                    removeOptionalBadge("Hiking", "Cycling");
                }
                else if (completedname.Equals("Hiking"))
                {
                    //x
                    for (int i = 0; i < eaglechecks.Length; i++)
                    {
                        try
                        {
                            string mbnamestring = (string)eaglechecks[i].Content;
                            if (mbnamestring.Equals("Swimming"))
                            {
                                eaglechecks[i].Visibility = System.Windows.Visibility.Visible;
                            }
                            else if (mbnamestring.Equals("Cycling"))
                            {
                                eaglechecks[i].Visibility = System.Windows.Visibility.Visible;
                            }
                        }
                        catch (NullReferenceException)
                        {

                            eaglechecks[i] = new CheckBox();
                            //eaglechecks[i].Content = "Swimming";
                            eaglechecks[i].Content = "Cycling";
                            eaglechecks[i].Checked += new RoutedEventHandler(eaglecheck_Checked);
                            eaglechecks[i].Unchecked += new RoutedEventHandler(eaglecheck_Unchecked);
                            //mbrequiredpanel.Children.Insert(10, eaglechecks[i]);
                            mbrequiredpanel.Children.Insert(12, eaglechecks[i]);
                            /*
                            eaglechecks[i + 1] = new CheckBox();
                            eaglechecks[i + 1].Content = "Cycling";
                            eaglechecks[i + 1].Checked += new RoutedEventHandler(eaglecheck_Checked);
                            eaglechecks[i + 1].Unchecked += new RoutedEventHandler(eaglecheck_Unchecked);
                             */
                            //mbrequiredpanel.Children.Insert(12, eaglechecks[i + 1]);
                            break;
                        }
                    }
                    removeOptionalBadge("Swimming", "Cycling");
                }
                else if (completedname.Equals("Cycling"))
                {
                    //x
                    for (int i = 0; i < eaglechecks.Length; i++)
                    {
                        try
                        {
                            string mbnamestring = (string)eaglechecks[i].Content;
                            if (mbnamestring.Equals("Swimming"))
                            {
                                eaglechecks[i].Visibility = System.Windows.Visibility.Visible;
                            }
                            else if (mbnamestring.Equals("Hiking"))
                            {
                                eaglechecks[i].Visibility = System.Windows.Visibility.Visible;
                            }
                        }
                        catch (NullReferenceException)
                        {
                            eaglechecks[i] = new CheckBox();
                            eaglechecks[i].Content = "Swimming";
                            eaglechecks[i].Checked += new RoutedEventHandler(eaglecheck_Checked);
                            eaglechecks[i].Unchecked += new RoutedEventHandler(eaglecheck_Unchecked);
                            mbrequiredpanel.Children.Insert(10, eaglechecks[i]);
                            eaglechecks[i + 1] = new CheckBox();
                            eaglechecks[i + 1].Content = "Hiking";
                            eaglechecks[i + 1].Checked += new RoutedEventHandler(eaglecheck_Checked);
                            eaglechecks[i + 1].Unchecked += new RoutedEventHandler(eaglecheck_Unchecked);
                            mbrequiredpanel.Children.Insert(11, eaglechecks[i + 1]);
                            break;
                        }
                    }
                    removeOptionalBadge("Swimming", "Hiking");
                }
            }
        }

        private void removeOptionalBadge(string badgename)
        {
            //x
            optlist.ItemsSource = null;
            //List<Group<CheckBox>> groupselection = optlist.ItemsSource.Cast<Group<CheckBox>>().ToList();
            int addpos = groupselection.Select((item, j) => new { Item = item, Index = j })
                .Single(x => x.Item.Key.Equals(badgename.Substring(0,1))).Index;
            /*
            var badgeremove = from query in groupselection[addpos] where (query.Content as string).Contains(badgename) select query;
            groupselection[addpos].Remove(badgeremove.First());
            badgeremove = null;
             */
            int delpos = groupselection[addpos].Select((item, j) => new { Item = (string)item.Content, Index = j })
                .Single(x => x.Item.Equals(badgename)).Index;
            groupselection[addpos][delpos].Checked -= new RoutedEventHandler(optioncheck_Checked);
            groupselection[addpos][delpos].Unchecked -= new RoutedEventHandler(optioncheck_Unchecked);
            groupselection[addpos].RemoveAt(delpos);
            
            optlist.ItemsSource = groupselection;

            if (completedoptionalbadges.Contains(badgename))
            {
                completedoptionalbadges.Remove(badgename);
            }
        }

        private void removeOptionalBadge(string badgename1, string badgename2)
        {
            //x
            //List<Group<CheckBox>> listitems = optlist.ItemsSource.Cast<Group<CheckBox>>().ToList();
            optlist.ItemsSource = null;
            int addpos = groupselection.Select((item, j) => new { Item = item, Index = j })
                .Single(x => x.Item.Key.Equals(badgename1.Substring(0, 1))).Index;
            /*
            var badgeremove = from query in groupselection[addpos] where (query.Content as string).Contains(badgename1) select query;
            //var badgeremove = from query in deletegroup where (query.Content as string).Contains(badgename1) select query;
            //groupselection[addpos].Remove(badgeremove.FirstOrDefault());
            MessageBox.Show("length of list (or array) is: " + groupselection[addpos].Count);
            groupselection[addpos].Remove(badgeremove.ElementAt(0));
            //deletegroup.Remove(badgeremove.FirstOrDefault());
             */
            int delpos = groupselection[addpos].Select((item, j) => new { Item = (string)item.Content, Index = j })
                .Single(x => x.Item.Equals(badgename1)).Index;
            groupselection[addpos][delpos].Checked -= new RoutedEventHandler(optioncheck_Checked);
            groupselection[addpos][delpos].Unchecked -= new RoutedEventHandler(optioncheck_Unchecked);
            groupselection[addpos].RemoveAt(delpos);
            addpos = groupselection.Select((item, j) => new { Item = item, Index = j })
                .Single(x => x.Item.Key.Equals(badgename2.Substring(0, 1))).Index;
            /*
            badgeremove = null;
            badgeremove = from query in groupselection[addpos] where (query.Content as string).Contains(badgename2) select query;
            groupselection[addpos].Remove(badgeremove.ElementAt(0));
            badgeremove = null;
            //deletegroup = null;
             */
            delpos = groupselection[addpos].Select((item, j) => new { Item = (string)item.Content, Index = j })
                .Single(x => x.Item.Equals(badgename2)).Index;
            groupselection[addpos][delpos].Checked -= new RoutedEventHandler(optioncheck_Checked);
            groupselection[addpos][delpos].Unchecked -= new RoutedEventHandler(optioncheck_Unchecked);
            groupselection[addpos].RemoveAt(delpos);
            optlist.ItemsSource = groupselection;
            //optlist.SelectedItems = prevselect;

            if (completedoptionalbadges.Contains(badgename1))
            {
                completedoptionalbadges.Remove(badgename1);
            }
            if (completedoptionalbadges.Contains(badgename2))
            {
                completedoptionalbadges.Remove(badgename2);
            }
        }

        protected override void OnNavigatedFrom(System.Windows.Navigation.NavigationEventArgs e)
        {
            saveState();
        }

        public void saveState()
        {
            //x
            /*
            using (IsolatedStorageFile appstore = IsolatedStorageFile.GetUserStoreForApplication())
            {
                using (IsolatedStorageFileStream xmlstream = appstore.CreateFile("scout_settings.xml"))
                {
                    foreach (XElement xe in loadeddata.Element("Scout").Element("MeritBadges").Nodes())
                    {
                        if (completedeaglebadges.Contains(xe.Value) || completedoptionalbadges.Contains(xe.Value))
                        {
                            //x
                            xe.Attribute("recieved").Value = "true";
                        }
                        else
                        {
                            xe.Attribute("recieved").Value = "false";
                        }
                    }
                    loadeddata.Save(xmlstream);
                    xmlstream.Close();
                }
            }
             */
            foreach (XElement xe in loadeddata.Element("Scout").Element("MeritBadges").Nodes())
            {
                if (completedeaglebadges.Contains(xe.Value) || completedoptionalbadges.Contains(xe.Value))
                {
                    //x
                    xe.Attribute("recieved").Value = "true";
                }
                else
                {
                    xe.Attribute("recieved").Value = "false";
                }
            }
            StorageHelper.saveSettingsXml("scout_settings.xml", loadeddata);
        }

        private class GroupComp : IComparer<Group<CheckBox>>
        {
            //x
            public int Compare(Group<CheckBox> a, Group<CheckBox> b)
            {
                //x
                string string1 = a.Key;
                string string2 = b.Key;
                if (string1.CompareTo(string2) != 0)
                {
                    //x
                    return string1.CompareTo(string2);
                }
                else
                {
                    return 0;
                }
            }
        }
    }
}